## lowfantasygaming

This version of the Low Fantasy Gaming system for Foundry VTT has been coded with permission for distribution
from Pickpocket Press. It includes a character sheet and system support for Low Fantasy Gaming.

## To Install

Simply install through the FoundryVTT "Game Systems" tab. If you'd prefer, you can also download the .zip file or clone the repository and extract it into Foundry's Data/systems/lfg folder. *After extracting, make sure the name of the containing folder is set to 'lfg' and not 'lfg-master'*

The Free PDF version of Low Fantasy Gaming is found at https://lowfantasygaming.com/freepdf/.

Code is modeled after the tutorial created by Matt Smith found at https://gitlab.com/asacolips-projects/foundry-mods/foundryvtt-system-tutorial using many of the files from the boilerplate system.

Any questions, comments, concerns, etc. may be emailed to development.devin@gmail.com or communicated through gitlab.

NEW DISCORD: https://discord.gg/ZvGfKPrRmy  Come join us to talk about future updates and current bugs. In all honesty, it's checked a bit more than the GitLab.

## Current Update (1.12.5)

Update Includes:

1) Updated all macros in the Useful Macros compendium to work with v12.

## Future Steps

1) Add an "Armour" item class that will interact with the owner's AC

2) Add in item and spell management for NPCs to help with automating attacks

3) Addition of Player Languages and compatibility with the Polyglot module
