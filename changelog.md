## Update 1.12.4

Update Includes:

1) Fixed bug where dragged items would not populate until refreshing the sheet.

2) Fixed bug where Notes would not persist.

## Update 1.12.3

Update Includes:

1) Fixed Items incompatibility with FVTT v12

## Update 1.12.2

Update Includes:

1) Fixes for deprecated items (evaluate({async}) and gridDistance/Units)

## Update 1.12.1

Update Includes:

1) Update to FoundryVTT v12

## Update 1.11.2

Update Includes:

1) Option to use PERC for to-hit and damage on ranged attacks.

2) Modifier dialog for NPC attacks and damage rolls.

## Update 1.11.1

Update Includes:

1) Testing for v11 compatibility

## Update 1.10.10

Update Includes:

1) Fixed bug where GS/TF values wouldn't update if a character was "skilled" in a specific skill.

2) Focusing on input fields now selects all of that input's text for better user experience. Users can still position their cursor on the second click.

## Update 1.10.9

Update Includes:

1) Fix for Bonus Rest maximum - used to incorrectly point at the WILL mod, changed to point to the correct CON mod.

## Update 1.10.8

Update Includes:

1) Added section in Equipment for Alternative Currency so that users can choose and record their own currencies.

2) Added HD Bonus input and modified HD roll to include bonus (for monsters with Hit Dice like "2+4HD").

3) Hit Dice roll now updates Current and Max HP when rolled. Preferred method is to roll HD from an unlinked NPC token, though it can be done from the Actor sidebar as well.

4) Included "1d4 HD" and "1d6 HD" for monsters with less than 1 HD. This only works on one selected token at a time to reduce accidentally selecting multiple monsters of different HD and updating them all.

5) Fixed bug where Custom Skills were not rolling after implementing the Modifier Dialog Box.

## Update 1.10.6

Update Includes:

1) Readability Improvements for Actor Sheets. Lighter bg image, bolder font used for small text (Amaranth Regular, provided under Open Font license: https://www.fontsquirrel.com/license/amaranth), and text wrapping for items/features.

2) Added Alternative Diagonal Movement option in Settings. To change, just go to the Game Settings tab > Configure Settings > Low Fantasy Gaming and pick between 5, 5, 5 and 5, 10, 5.

## Update 1.10.5
Update Includes:

1) Dialogs for attribute, skill, and luck rolls, allowing for situational modifiers and adv/disadv

2) Dialogs for damage rolls (Characters only for now) that allow for situational modifiers and adv/disadv

3) **Deluxe Initiative Macro** now added into "Useful Macros" compendium. With this macro, you just select all tokens you wish to have in initiative and click the button on the macro bar. This will roll DEX checks for all selected tokens (except for NPCs) and assign them a 2 (Great Success), 1 (Success), or 0 (Failure). NPCs are automatically added at 0.5 in initiative order. I'm still figuring out the kinks on multiple initiative points for Boss Monsters and if that's even possible right now.

4) All **Deluxe** classes now have their own Class Features compendium (Thanks for the permission, Steve!). Artwork for these was, once again, provided by game-icons.net

## Update 1.10.4
Update Includes:

1) Bug fix where Actor Name was showing as "undefined" during weapon attack rolls

2) Fixed weapon attack chat card formatting issue

## Update 1.10.3

Update Includes:

1) Updated NPC sheets with 4 Attack/Damage inputs and a larger notes section

2) Automated NPC Attacks

## Update 1.10.2

Update Includes:

1) Fix for NPC sheets not rendering after default sheet was changed

## Update 1.10.1

Update Includes:

1) Full compatibility with v10 (except for the original character sheet).

2) Test Sheet is now the default character sheet for new characters. The original sheet has been moved to be a secondary and will be discontinued at the FoundryVTT v12 launch once its data structure is no longer supported.

3) New versioning system - this will now match up with FVTT releases. Current version (1.10.1) shows base.FVTTversion.UpdateNumber (so base, FVTT v10, update 1). When I release the version no longer containing the original character sheet, the version will end up becoming 2.12.1

## Update 1.3.6

Update Includes:

1) Automated Rests!

2) Short Rests rolls selected number of WILL checks, counts the successes, and allows user to choose which benefits to gain. Also includes the ability to use Bonus Checks

3) Long Rests update all attribute loss, expended reroll dice, expended class abilities, recover 1 point of luck, and recover 1/2 lost HP plus 1d4+CON mod.

## Update 1.3.5

Update Includes:

1) Fixed a layout bug where long names would jump lines, causing readability issues.

2) Replaced the "Name" above each list with the List Title, making each list far less busy.

## Update 1.3.4

Update Includes:

1) A brand new character sheet layout! This one feels loads better and includes Custom Skills, Easily Tracked Class Abilities, and the Attributes and Skills are in constant view, so much less tab hopping.

2) NPC sheets now have "Base" and "Current" attribute tallies for easier tracking, modeled after the new character sheet.

3) A fresh new background texture has been added to the sheets. The base image was from Kiwihug (https://unsplash.com/@kiwihug) and was edited by me.

4) A changelog.md has been added and all previous version notes have been moved there for a cleaner GitLab page. This also means that I can add "Future Steps" here and it won't be lost in a sea of text.

## Version 1.3.3

Update Includes:

1) Fix compatibility issue with PopOut! module where weapon rolls were not possible when character sheet is popped out.

## Version 1.3.2

Update Includes:

1) Fix for "weapon-attack" and "useful-macros" macros not working with new roll structure

2) Changed all rolls to asynchronous to future-proof

## Version 1.2.9

Update Includes:

1) Fix issue with ranged weapons using same formulas for both advantage and disadvantage

## Version 1.2.8

Update Includes:

1) Changes for use with v9 of FoundryVTT

## Version 1.2.7

Update Includes:

1) Tested for compatability with 0.8.9

2) Started v9 updates for easier transition when it becomes stable

3) Fixed bug where Earth Elemental from the Monsters compendium showed up during play as an Air Elemental

4) NPC/Monster Luck does not decrement on a successful check, as per LFG RAW

5) Terrible Failures for attribute checks are now capped at 20

6) Items, Effects, Spells, etc. are now able to be reordered by dragging and dropping

## Version 1.2.6

Update Includes:

1) Fixed bug where Target was always 10 on Luck and Attribute rolls

## Version 1.2.5

Update Includes:

1) Fixed NPC Image bug where Image was not editable.

## Version 1.2.4

NOTE: The minimum compatible version for this update is 0.8.5. If you are still running 0.7.x, do not update. You can find older versions in the "tags."

Update Includes:

1) Updated weapon rolls from sheet. Now, when you click on your weapon, a dialog pops up where you can input situational modifiers and roll with advantage and disadvantage. The damage rolls are now done from the chat in order to keep the chat log clean - no more useless damage rolls when the attack doesn't hit. Highlighting is still present, and Nat19's don't happen on ranged weapons as per RAW.

2) Added the ability to roll your attribute checks and luck rolls with adv/disadv by modifying your mouse click. "Shift + Click" in order to roll your checks with advantage. "Alt + Click" in order to roll your checks with disadvantage.

3) Added Ammo/Charges/Number to weapons. The Ammo/Charges/Number of weapons and the Quantity of items is now displayed next to the item on the actor sheet. You can also add/subtract directly from the actor sheet instead of having to open up the item/weapon sheet.

## Version 1.2.3

Update (skipping 1.2.1 and 1.2.3 due to misclicks) Includes:

1) A quick fix to the new data structure for 0.8.x. This is likely to change to a cleaner version in future updates

2) Tested for Compatibility with 0.8.6

## Version 1.2.0

Update Includes:

1) The following compendiums from LeonardoDaLychee: Spells, Spells (Deluxe Names), Class Abilities (Free PDF Classes), Madness Effects, DDM Effects, and Injuries and Setbacks

2) Images for the compendiums used with permission from game-icons.net and Giddy

3) Deleted d20 image from items so that the item images would show on the character sheet

4) Input boxes for Temp AC and Temp HP

5) Divided the Skills and Attributes into two tabs for easier readability on tablets

6) Added a "base" input for Attributes to allow for easier tracking of temp Attribute drains and buffs. All Attribute scores used in calculations still follow @data.abilities.{attr}.value

7) Added Favour Checkbox for use with the Cultist Class from Deluxe

## Version 1.1.9

Changed NPC Luck for easier use with Macros

## Version 1.1.8

Update includes:

1) Added Luck and DDM macros for rolling outside of the character sheet as well as for use in future Spell Compendiums

2) Fixed Spell Item Sheet typo not allowing dragging and dropping of spells

3) Added "Example Poisons" rollable table that I had missed in a previous update

## Version 1.1.7

Update includes:

1) Fixed Weapon Attack Macro that allows deselecting Advantage and Disadvantage

2) Success and Failure highlighting for attribute and Luck checks

## Version 1.1.6

(Skipped over 1.1.5 due to an error in calculating critical damage)

Update includes:

1) Code fixes for Luck Roll compatibility with PopOut! module, fixed Detection Skill, fixed first Spell Slot input for each level, fixed text in Whip Weapon Attack Macro

2) Added Luck Dropdown to NPC sheet, made Hit Dice and Number Appearing rollable from sheet

3) Replaced Item Weight with Item Cost and made it rollable from the sheet

4) Moved Crit Range from Item Sheet to Character Sheet

5) Weapons and Items compendiums from @Karfumble

6) Weapon Attack Macro from @Tom Straszewski (choose weapons from dropdown, with adjustable modifiers, advantage and disadvantage, and +1 to damage for attacking with two hands)

7) "Useful Macros" compendium, including above Weapon Attack Macro, deduct spell slots by level, reset spell slots after long rest, and Dex Based Initiative rolls

8) Weapons, Spells, and Items are now rollable from the sheet. Weapons rolled from the sheet include highlighted Crits and Fumbles as well as Nat19 effects. Spells list all important information. Items show name and properties.

## Version 1.1.4

Update includes:

1) Improved DDM Rolls. DDM Target is now displayed properly instead of a constant "10" and success/failure is now noted. Additionally, DDM Target automatically increases by +1 on a success and resets to 1 on a failure (while also prompting the owner to roll on the DDM Table)

2) A compendium of all Roll Tables from the Free PDF version of Low Fantasy Gaming, thanks to Alain Choiniere! Most everything is auto-rolled for the one rolling, from damage to number appearing. Luck Saves and Percentages do not auto-roll.

## Version 1.1.3

Update includes:

1) Adding Spell Slots for tracking purposes

## Version 1.1.2

Update includes:

1) Tested for compatibility with FoundryVTT version 0.7.9

2) Improved Skills list, with appropriate rolls, coded by Guin Jane

3) Improved Luck Rolls with selectable attribute and automatic Luck reduction on successful checks coded by Guin Jane

## Version 1.1.1

Update includes:

1) Tested for compatibility with FoundryVTT version 0.7.7 and updated in system.json

2) Improved attribute rolls, including levels of success, coded by Guin Jane

## Version 1.1.0

Update includes:

1) Tested for compatibility with FoundryVTT version 0.7.7 and updated in system.json

2) Added NPC actor type for simpler sheets. For use with both NPCs and Monsters

3) Added Monster Compendium for the Free PDF version of Low Fantasy Gaming, with included place-holder images from game-icons.net

4) Added Weapon Attacks (Macro) compendium, with included place-holder images from game-icons.net

## Version 1.0.2

Small update including:

1) Tested for compatibility with FoundryVTT version 0.7.6 and updated in system.json

2) Added svg icons for weapon macros (macros coming in next update)

## Version 1.0.1

This small update includes the following:

1) Tested for compatibility with FoundryVTT version 0.7.5 and updated in system.json

2) Updated manifest to fix issue with checking for updates through FoundryVTT's Game Systems tab

3) Changed default token image to LFG icon provided by Pickpocket Press and edited with permission

4) Changed default font to Aquifer to match LFG branding