export default function registerSystemSettings() {
    // Diagonal movement rule
    game.settings.register("lfg", "diagonalMovement", {
        name: "Diagonal Movement",
        hint: "Change Diagonal Movement between Standard (5, 5, 5) and Alt (5, 10, 5)",
        scope: "world",
        config: true,
        default: "555",
        type: String,
        choices: {
            555: "5, 5, 5",
            5105: "5, 10, 5"
        },
        onChange: rule => canvas.grid.diagonalRule = rule
    });

    // Perc as Ranged Mod
    game.settings.register("lfg", "percRanged", {
        name: "Ranged Modifier",
        hint: "Choose between DEX (Standard Rules) and PERC (Modified Rules) as the modifier for ranged attacks.",
        scope: "world",
        config: true,
        default: "dex",
        type: String,
        choices: {
            dex: "Dex",
            perc: "Perc"
        }
    });
}