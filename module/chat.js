export function chatListeners(html) {
    html.on('click', '.melee-damage', onMeleeDamage);
    html.on('click', '.ranged-damage', onDexDamage);
    html.on('click', '.npc-damage', onNpcDamage);
    html.on('click', '.regain-hp', onRegainHP);
    html.on('click', '.regain-ability', onRegainAbility);
    html.on('click', '.regain-reroll', onRegainReroll);
    html.on('click', '.bonus-rest', onBonusRest);
}

function onMeleeDamage(event) {
    const damage = event.currentTarget.getAttribute('data-dmg');
    const str = event.currentTarget.getAttribute('data-str');

    //add in dialog for damage modifiers
    const modDialog = new Dialog({
      title: "Damage Modifiers",
      content: `<html>
        <p>These modifiers are <b>IN ADDITION</b> to your STR mod. If your weapon does a constant +1 damage, please put that in the Damage input instead of having to enter it here each time.</p>
        <table class="section-table">
        <tr>
          <td><label for="sitMod">Additional Damage Modifier</label></td>
          <td><input class="sitMod" type="text" id="sitMod" placeholder="e.g.: 1 or -2"></td>
        </tr>
        </table>
        </html>`,
      buttons: {
        button1: {
          label: "Disadvantage",
          callback: (html) => disadRoll(html)
        },
        button2: {
          label: "Normal Roll",
          callback: (html) => normalRoll(html)
        },
        button3: {
          label: "Advantage",
          callback: (html) => advRoll(html)
        }
      },
      default: "button2"
    }).render(true);

    async function disadRoll(html) {
        let sitMod = html.find("input#sitMod").val();
        let rollFormula;
        if (sitMod == "") {
            rollFormula = `{${damage}+${str}, ${damage}+${str}}kl`;
        } else {
            rollFormula = `{${damage}+${str}+${sitMod}, ${damage}+${str}+${sitMod}}kl`;
        }
        let dmg = new Roll(rollFormula);
            await dmg.evaluate({async: false});
            dmg.toMessage({
            speaker: ChatMessage.getSpeaker(),
            flavor: "Damage Roll - Disadvantage"
        });
    }

    async function normalRoll(html) {
        let sitMod = html.find("input#sitMod").val();
        let rollFormula;
        if (sitMod == "") {
          rollFormula = `${damage}+${str}`;
        } else {
          rollFormula = `${damage}+${str}+${sitMod}`;
        }
        let dmg = new Roll(rollFormula);
          await dmg.evaluate({async: false});
          dmg.toMessage({
          speaker: ChatMessage.getSpeaker(),
          flavor: "Damage Roll"
      });
    }

    async function advRoll(html) {
        let sitMod = html.find("input#sitMod").val();
        let rollFormula;
        if (sitMod == "") {
          rollFormula = `{${damage}+${str}, ${damage}+${str}}kh`;
        } else {
          rollFormula = `{${damage}+${str}+${sitMod}, ${damage}+${str}+${sitMod}}kh`;
        }
        let dmg = new Roll(rollFormula);
          await dmg.evaluate({async: false});
          dmg.toMessage({
          speaker: ChatMessage.getSpeaker(),
          flavor: "Damage Roll - Advantage"
      });
    }
}

function onDexDamage(event) {
    const damage = event.currentTarget.getAttribute('data-dmg');
    const dex = event.currentTarget.getAttribute('data-dex');
    const rangedPerc = game.settings.get("lfg", "percRanged");
    console.log("DAMAGE: " + damage + " DEX: " + dex + " rangedPerc: " + rangedPerc);

    let content = `<html>
      <p>These modifiers are <b>IN ADDITION</b> to your DEX mod. If your weapon does a constant +1 damage, please put that in the Damage input instead of having to enter it here each time.</p>
      <table class="section-table">
      <tr>
        <td><label for="sitMod">Additional Damage Modifier</label></td>
        <td><input class="sitMod" type="text" id="sitMod" placeholder="e.g.: 1 or -2"></td>
      </tr>
      </table>
      </html>`;
    
    if (rangedPerc == "perc") {
      content = `<html>
      <p>These modifiers are <b>IN ADDITION</b> to your PERC mod. If your weapon does a constant +1 damage, please put that in the Damage input instead of having to enter it here each time.</p>
      <table class="section-table">
      <tr>
        <td><label for="sitMod">Additional Damage Modifier</label></td>
        <td><input class="sitMod" type="text" id="sitMod" placeholder="e.g.: 1 or -2"></td>
      </tr>
      </table>
      </html>`;
    }

    //add in dialog for damage modifiers
    const modDialog = new Dialog({
        title: "Damage Modifiers",
        content: content,
        buttons: {
          button1: {
            label: "Disadvantage",
            callback: (html) => disadRoll(html)
          },
          button2: {
            label: "Normal Roll",
            callback: (html) => normalRoll(html)
          },
          button3: {
            label: "Advantage",
            callback: (html) => advRoll(html)
          }
        },
        default: "button2"
      }).render(true);

      async function disadRoll(html) {
        let sitMod = html.find("input#sitMod").val();
        let rollFormula;
        if (sitMod == "") {
            rollFormula = `{${damage}+${dex}, ${damage}+${dex}}kl`;
        } else {
            rollFormula = `{${damage}+${dex}+${sitMod}, ${damage}+${dex}+${sitMod}}kl`;
        }
        let dmg = new Roll(rollFormula);
            await dmg.evaluate();
            dmg.toMessage({
            speaker: ChatMessage.getSpeaker(),
            flavor: "Damage Roll - Disadvantage"
        });
    }

    async function normalRoll(html) {
        let sitMod = html.find("input#sitMod").val();
        let rollFormula;
        if (sitMod == "") {
          rollFormula = `${damage}+${dex}`;
        } else {
          rollFormula = `${damage}+${dex}+${sitMod}`;
        }
        let dmg = new Roll(rollFormula);
          await dmg.evaluate();
          dmg.toMessage({
          speaker: ChatMessage.getSpeaker(),
          flavor: "Damage Roll"
      });
    }

    async function advRoll(html) {
        let sitMod = html.find("input#sitMod").val();
        let rollFormula;
        if (sitMod == "") {
          rollFormula = `{${damage}+${dex}, ${damage}+${dex}}kh`;
        } else {
          rollFormula = `{${damage}+${dex}+${sitMod}, ${damage}+${dex}+${sitMod}}kh`;
        }
        let dmg = new Roll(rollFormula);
          await dmg.evaluate();
          dmg.toMessage({
          speaker: ChatMessage.getSpeaker(),
          flavor: "Damage Roll - Advantage"
      });
    }
}

function onNpcDamage(event) {
    const damage = event.currentTarget.getAttribute('data-dmg');
    let content = `<html>
    <p>These modifiers are <b>IN ADDITION</b> to the NPC's standard damage. If the NPC's does a constant +1 damage, please put that in the Damage input instead of having to enter it here each time.</p>
    <table class="section-table">
    <tr>
      <td><label for="sitMod">Additional Damage Modifier</label></td>
      <td><input class="sitMod" type="text" id="sitMod" placeholder="e.g.: 1 or -2"></td>
    </tr>
    </table>
    </html>`;

    //add in dialog for damage modifiers
    const modDialog = new Dialog({
      title: "Damage Modifiers",
      content: content,
      buttons: {
        button1: {
          label: "Disadvantage",
          callback: (html) => disadRoll(html)
        },
        button2: {
          label: "Normal Roll",
          callback: (html) => normalRoll(html)
        },
        button3: {
          label: "Advantage",
          callback: (html) => advRoll(html)
        }
      },
      default: "button2"
    }).render(true);

    async function disadRoll(html) {
      let sitMod = html.find("input#sitMod").val();
      let rollFormula;
      if (sitMod == "") {
          rollFormula = `{${damage}, ${damage}}kl`;
      } else {
          rollFormula = `{${damage}+${sitMod}, ${damage}+${sitMod}}kl`;
      }
      let dmg = new Roll(rollFormula);
          await dmg.evaluate({async: false});
          dmg.toMessage({
          speaker: ChatMessage.getSpeaker(),
          flavor: "Damage Roll - Disadvantage"
      });
  }

  async function normalRoll(html) {
      let sitMod = html.find("input#sitMod").val();
      let rollFormula;
      if (sitMod == "") {
        rollFormula = `${damage}`;
      } else {
        rollFormula = `${damage}+${sitMod}`;
      }
      let dmg = new Roll(rollFormula);
        await dmg.evaluate({async: false});
        dmg.toMessage({
        speaker: ChatMessage.getSpeaker(),
        flavor: "Damage Roll"
    });
  }

  async function advRoll(html) {
      let sitMod = html.find("input#sitMod").val();
      let rollFormula;
      if (sitMod == "") {
        rollFormula = `{${damage}, ${damage}}kh`;
      } else {
        rollFormula = `{${damage}+${sitMod}, ${damage}+${sitMod}}kh`;
      }
      let dmg = new Roll(rollFormula);
        await dmg.evaluate({async: false});
        dmg.toMessage({
        speaker: ChatMessage.getSpeaker(),
        flavor: "Damage Roll - Advantage"
    });
  }
}

function onRegainHP(event) {
    //grabbing actor
    const id = event.currentTarget.getAttribute('data-actor');
    let actor = game.actors.get(id);
    
    //grabbing other data from button
    let currentHP = Number(event.currentTarget.getAttribute('data-current-hp'));
    let maxHP = Number(event.currentTarget.getAttribute('data-max-hp'));
    let con = Number(event.currentTarget.getAttribute('data-con'));
    let will = Number(event.currentTarget.getAttribute('data-will'));
    let bonusChecks = actor.system.resources.bonusRests.value;
    let bonusChecksMax = actor.system.abilities.will.mod;

    //finding new HP amount
    let hpDiff = maxHP - currentHP;
    let hpAdd = Math.floor(hpDiff/2) + con;
    let newValue = hpAdd + currentHP;
    if (newValue > maxHP) {
        newValue = maxHP;
    }
    actor.update({"system.health.value": newValue});

    //sending Chat Message
    ChatMessage.create({
        content: `<html>
                ${actor.name} regains <b>${hpAdd} HP</b><br>
                They have <b>${bonusChecks}</b> of ${bonusChecksMax} bonus checks remaining.<br>
                Use a bonus check?<hr>
                <button class="bonus-rest"
                    data-actor="${id}"
                    data-will="${will}"
                    data-bonus-checks="${bonusChecks}"
                    data-bonus-checks-max="${bonusChecksMax}">Use Bonus</button>
                </html>`
    });
}

function onRegainAbility(event) {
    //grabbing actor
    const id = event.currentTarget.getAttribute('data-actor');
    let actor = game.actors.get(id);

    //grabbing other data
    let will = Number(event.currentTarget.getAttribute('data-will'));
    let bonusChecks = actor.system.resources.bonusRests.value;
    let bonusChecksMax = actor.system.abilities.will.mod;

    //sending Chat Message
    ChatMessage.create({
        content: `<html>
                ${actor.name} may regain <b>1 Use</b> of an expended Class Ability.<br>
                NOTE that Magic Users may not recover an expended spell use from the same spell level more than once within 24 hours.<br><br>
                They have <b>${bonusChecks}</b> of ${bonusChecksMax} bonus checks remaining.<br><br>
                Use a bonus check?<hr>
                <button class="bonus-rest"
                    data-actor="${id}"
                    data-will="${will}"
                    data-bonus-checks="${bonusChecks}"
                    data-bonus-checks-max="${bonusChecksMax}">Use Bonus</button>
                </html>`
    });
}

function onRegainReroll(event) {
    //grabbing actor
    const id = event.currentTarget.getAttribute('data-actor');
    let actor = game.actors.get(id);

    //grabbing other data
    let will = Number(event.currentTarget.getAttribute('data-will'));
    let bonusChecks = actor.system.resources.bonusRests.value;
    let bonusChecksMax = actor.system.abilities.will.mod;

    //update reroll die
    actor.update({"system.resources.reroll.value": actor.system.resources.reroll.value + 1});

    //sending Chat Message
    ChatMessage.create({
        content: `<html>
                ${actor.name} has regained <b>1</b> expended Reroll Die.<br>
                They have <b>${bonusChecks}</b> of ${bonusChecksMax} bonus checks remaining.<br>
                Use a bonus check?<hr>
                <button class="bonus-rest"
                    data-actor="${id}"
                    data-will="${will}"
                    data-bonus-checks="${bonusChecks}"
                    data-bonus-checks-max="${bonusChecksMax}">Use Bonus</button>
                </html>`
    });
}

async function onBonusRest(event) {
    //grabbing actor
    const id = event.currentTarget.getAttribute('data-actor');
    let actor = game.actors.get(id);
    let actorData = actor.system;

    //get current and max HP, as well as Con Mod
    let currentHP = actorData.health.value;
    let maxHP = actorData.health.max;
    let conMod = actorData.abilities.con.mod;

    //get current WILL value to count successess when rolling
    let will = actorData.abilities.will.value;

    //subtracting bonus rest
    actor.update({"system.resources.bonusRests.value": actorData.resources.bonusRests.value - 1});

    let check = await new Roll(`1d20cs<=${will}`).evaluate();
    check.toMessage({
        speaker: ChatMessage.getSpeaker(),
        flavor: `<html>
                <img style="width: 50px; height: 50px; float: left; margin: 3px 5px 0 0;" src="${actor.img}">
                ${actor.name} takes some time to recover.<hr>
                They may gain <b>${check.total}</b> short rest benefit(s).<br>
                Choose your benefit(s).<hr>
                <button class="regain-hp"
                  data-actor="${actor.id}"
                  data-current-hp="${currentHP}"
                  data-max-hp="${maxHP}"
                  data-con="${conMod}"
                  data-will="${will}"
                  data-bonus-checks="${actorData.resources.bonusRests.value}"
                  data-bonus-checks-max="${actorData.abilities.will.mod}">Regain 1/2 HP + CON Mod</button>
                <button class="regain-ability"
                  data-actor="${actor.id}"
                  data-bonus-checks="${actorData.resources.bonusRests.value}"
                  data-bonus-checks-max="${actorData.abilities.will.mod}">Regain 1 Ability Use</button>
                <button class="regain-reroll"
                  data-actor="${actor.id}"
                  data-reroll-current="${actorData.resources.reroll.value}"
                  data-reroll-max="${actorData.resources.reroll.max}"
                  data-bonus-checks="${actorData.resources.bonusRests.value}"
                  data-bonus-checks-max="${actorData.abilities.will.mod}">Regain 1 Reroll Die</button>
                <button class="bonus-rest"
                  data-actor="${id}"
                  data-will="${will}"
                  data-bonus-checks="${actorData.resources.bonusRests.value}"
                  data-bonus-checks-max="${actorData.abilities.will.mod}">Use Bonus</button>
                  <hr>
                </html>`
      });
}