/**
 * Extend the base Actor entity by defining a custom roll data structure.
 * @extends {Actor}
 */

import {  getModGsTs } from "../lfgutils.js";

export class LFGActor extends Actor {

  /**
   * Augment the basic actor data with additional dynamic data.
   */
  prepareData() {
    super.prepareData();

    // Make separate methods for each Actor type (character, npc, etc.) to keep
    // things organized.
    if (this.type === 'character') this._prepareCharacterData(this);
    
    if (this.type === 'npc') this._prepareNpcData(this);
  }

  /**
   * Prepare Character type specific data
   */
  _prepareCharacterData(actorData) {
    const data = actorData.system;
    // Make modifications to data here.
    // // Loop through ability scores, and add their modifiers to our sheet output.
    for (let [key, ability] of Object.entries(data.abilities)) {
      data.abilities[key] = { ...ability, ...getModGsTs(ability.value) };
    }
    for (let [key, skill] of Object.entries(data.skills)) {
      if (data.skills[key].skilled) {
        data.skills[key] = { ...skill, ...getModGsTs(data.abilities[skill.attr].value + 1) };
      } else {
        data.skills[key] = { ...skill, ...getModGsTs(data.abilities[skill.attr].value) };
      }
    }
  }

  _prepareNpcData(actorData) {
    const data = actorData.system;
    // Make modifications to data here.
    // Loop through ability scores, and add their modifiers to our sheet output.
    for (let [key, ability] of Object.entries(data.abilities)) {
      data.abilities[key] = { ...ability, ...getModGsTs(ability.value) };
    }
  }

}