/**
 * Extend the basic ActorSheet with some very simple modifications
 * @extends {ActorSheet}
 */
import { lfgRoll, ddmRoll, ddmMessage, successLevelMessage } from "../lfgutils.js";
import { weaponDialogContent } from "../lfg-dialog.js"

export class LFGActorSheet extends ActorSheet {
  /** @override */
  static get defaultOptions() {
    return mergeObject(super.defaultOptions, {
      classes: ["lfg", "sheet", "actor"],
      width: 900,
      height: 600,
      tabs: [{ navSelector: ".sheet-tabs", contentSelector: ".sheet-body", initial: "description" }]
    });
  }

  /** @override */
get template() {
  if (this.actor.type == 'character') {
    return `systems/lfg/templates/actor/actor-sheet.html`;
  } else if (this.actor.type == 'npc') {
    return `systems/lfg/templates/actor/npc-sheet.html`;
  }
}

  /* -------------------------------------------- */
  
  /** @override */
  getData() {
    const data = super.getData();
    data.dtypes = ["String", "Number", "Boolean"];

    // Prepare items.
    if (this.actor.type == 'character') {
      this._prepareCharacterItems(data);
    }

    if (this.actor.type == 'npc') {
      this._prepareNpcData(data);
    }

    return data;
  }

  /**
   * Organize and classify Items for Character sheets.
   *
   * @param {Object} actorData The actor to prepare.
   *
   * @return {undefined}
   */
  _prepareCharacterItems(sheetData) {
    const actorData = sheetData.actor;

    // Initialize containers.
    const gear = [];
    const weapons = [];
    const features = [];
    const skills = [];
    const spells = {
      1: [],
      2: [],
      3: [],
      4: [],
      5: [],
      6: []
    };
    const injSetbacks = [];
    const madness = [];
    const darkMagic = [];

    // Iterate through items, allocating to containers
    // let totalWeight = 0;
    for (let i of sheetData.items) {
      let item = i.system;
      i.img = i.img || DEFAULT_TOKEN;
      // Append to gear.
      if (i.type === 'item') {
        gear.push(i);
      }
      // Append to weapons.
      if (i.type === 'weapon') {
        weapons.push(i);
      }
      // Append to features.
      else if (i.type === 'feature') {
        features.push(i);
      }
      // Append to spells.
      else if (i.type === 'spell') {
        if (i.system.spellLevel != undefined) {
          spells[i.system.spellLevel].push(i);
        }
      }
       // Append to injuries and setbacks.
      else if (i.type === 'injSetback') {
        injSetbacks.push(i);
      }
      else if (i.type === 'madness') {
        madness.push(i);
      }
      else if (i.type === 'darkMagic') {
        darkMagic.push(i);
      }
    }

    // Assign and return
    actorData.gear = gear;
    actorData.weapons = weapons;
    actorData.features = features;
    actorData.skills = skills;
    actorData.spells = spells;
    actorData.injSetbacks = injSetbacks;
    actorData.madness = madness;
    actorData.darkMagic = darkMagic;
  }

  async _prepareNpcData(sheetData) {
    const actorData = sheetData.actor;

    //v10 Enriched Editor
    actorData.enrichedDescription = await TextEditor.enrichHTML(actorData.system.npcStats.description, {async: true});
  }

  /* -------------------------------------------- */

  /** @override */
  activateListeners(html) {
    super.activateListeners(html);

    // Everything below here is only needed if the sheet is editable
    if (!this.options.editable) return;

    html.find('.skill-proficient').on("click contextmenu", this._onToggleSkillProficiency.bind(this));

    // Add Inventory Item
    html.find('.item-create').click(this._onItemCreate.bind(this));

    // Update Inventory Item
    html.find('.item-edit').click(ev => {
      const li = $(ev.currentTarget).parents(".item");
      const item = this.actor.items.get(li.data("itemId"));
      item.sheet.render(true);
    });

    // Adding Quantity to Item
    html.find('.item-add').click(ev => {
      const li = $(ev.currentTarget).parents(".item");
      const item = this.actor.items.get(li.data("itemId"));
      const itemData = item.system;

      console.log(item.type);

      if (item.type == "weapon") {
        if (!itemData.ammo) {
          item.update({ 'data.ammo': 1});
        } else {
          item.update({ 'data.ammo': itemData.ammo + 1 });
        }
      }

      if (item.type == "item") {
        if (!itemData.quantity){
          item.update({ 'data.quantity': 1});
        } else {
          item.update({ 'data.quantity': itemData.quantity + 1});
        }
      }
    });

    // Subtracting Quantity from Item
    html.find('.item-subtract').click(ev => {
      const li = $(ev.currentTarget).parents(".item");
      const item = this.actor.items.get(li.data("itemId"));
      const itemData = item.system;

      console.log(item.type);

      if (item.type == "weapon") {
         item.update({ 'data.ammo': itemData.ammo - 1 });
        }

      if (item.type == "item") {
        item.update({ 'data.quantity': itemData.quantity - 1});
      }
    });

    // Send Item info to Chat
    html.find('.item-chat').click(ev => {
      const li = $(ev.currentTarget).parents(".item");
      const item = this.actor.items.get(li.data("itemId"));
      const itemData = item.system;

      console.log(itemData.properties);

      ChatMessage.create({
        content: `<html>
                  <div style="min-height:50px;"><img style="width:50px; height:50px; float:left; margin-right:5px; margin-top:3px;" src="${item.img}"> <h1 style="border-bottom:none;">${item.name}</h1></div>
                  <hr>
                  <div style="clear:left;">${itemData.properties}</div>
                  </html>`
      });
    });

    // Work Around for Spells to Chat
    html.find('.spell-chat').click(ev => {
      const li = $(ev.currentTarget).parents(".item");
      const item = this.actor.items.get(li.data("itemId"));
      const itemData = item.system;

      console.log(itemData.properties);

      ChatMessage.create({
        content: `<html>
                  <div style="min-height:50px;"><img style="width:50px; height:50px; float:left; margin-right:5px; margin-top:3px;" src="${item.img}"> <h1 style="border-bottom:none;">${item.name}</h1></div>
                  <hr>
                  <div style="clear:left;"><h4>Range: ${itemData.spellRange}, Duration: ${itemData.spellDuration}, Save: ${itemData.spellSave}</h4></div>
                  <hr>
                  <div>${itemData.properties}</div>
                  </html>`
      });
    });

    // Weapon Attacks to Chat
    this.element.find('.weapon-chat').click(ev => {

      // Getting Item and Actor Data
      const li = this.element.find(ev.currentTarget).parents(".item");
      const item = this.actor.items.get(li.data("itemId"));
      const itemData = item.system;
      const rollData = this.actor.getRollData();
      const actorData = this.actor.system;
      mergeObject(rollData, itemData);

      // Testing for Correct Data
      console.log(itemData);

      // Creating and Adjusting Additional Relevant Modifiers
      let attackType = itemData.type.toLowerCase();
      let crit;
      let lvl = Math.ceil(actorData.about.level.value / 2);

      // Assigning the Crit Value
      switch (actorData.attributes.critRange.value) {
        case "20":
          crit = 20;
          break;
        case "19-20":
          crit = 19;
          break;
        case "18-20":
          crit = 18;
      }

      switch (attackType) {
        case "melee":
          const meleeDialog = new Dialog({
            title: "Melee Attack Modifiers",
            content: weaponDialogContent,
            buttons: {
              button1: {
                label: "Disadvantage",
                callback: (html) => meleeDisad(html)
              },
              button2: {
                label: "Normal Attack",
                callback: (html) => meleeNorm(html)
              },
              button3: {
                label: "Advantage",
                callback: (html) => meleeAdv(html)
              }
            },
            default: "button2"
          }).render(true);

          async function meleeDisad(html) {
            let sitMod = html.find("input#sitMod").val();
            let disad = await new Roll("2d20kl + @rollData.abilities.str.mod + @rollData.attributes.attackBonus.value + @sitMod", {rollData, sitMod}).evaluate({async: true});
            let dieNum = disad.result[0] + disad.result[1];

            if (dieNum >= crit && dieNum != 19){
              // Get Max Damage for Crit
              let dmg = await new Roll("@rollData.damage", rollData).evaluate({async: true});
              let maxDmg = (dmg.dice[0].faces * dmg.dice[0].number) + rollData.abilities.str.mod + lvl;

              disad.toMessage({
                speaker: ChatMessage.getSpeaker(),
                flavor: `<html>
                <div style="min-height:50px;"><img style="width:50px; height:50px; float:left; margin-right:5px; margin-top:3px;" src="${item.img}"> <h3><b>${actorData.name}</b> attacks with their <b>${item.name}</b></h3>
                <hr>
                <i>${itemData.properties}</i></div>
                <div><b style="color: green;">CRITICAL HIT</b></div>
                </br>
                <div><b>MAX DAMAGE</b> &nbsp [[${maxDmg}]]</div>
                </html>`
              });
            } else if (dieNum >= crit && dieNum == 19){
              // Get Max Damage for Crit
              let dmg = await new Roll("@rollData.damage", rollData).evaluate({async: true});
              let maxDmg = (dmg.dice[0].faces * dmg.dice[0].number) + rollData.abilities.str.mod + lvl;

              disad.toMessage({
                speaker: ChatMessage.getSpeaker(),
                flavor: `<html>
                <div style="min-height:50px;"><img style="width:50px; height:50px; float:left; margin-right:5px; margin-top:3px;" src="${item.img}"> <h3><b>${actorData.name}</b> attacks with their <b>${item.name}</b></h3>
                <hr>
                <i>${itemData.properties}</i></div>
                <div><b style="color: green;">CRITICAL HIT</b></div>
                </br>
                <div><b>MAX DAMAGE</b> &nbsp [[${maxDmg}]]</div>
                <div><b style="color: green;">NATURAL 19</b></div>
                <div>${itemData.natNineteen}</div>
                </html>`
              });
            } else if (dieNum < crit && dieNum == 19) {
              disad.toMessage({
                speaker: ChatMessage.getSpeaker(),
                flavor: `<html>
                <div style="min-height:50px;"><img style="width:50px; height:50px; float:left; margin-right:5px; margin-top:3px;" src="${item.img}"> <h3><b>${actorData.name}</b> attacks with their <b>${item.name}</b></h3>
                <hr>
                <i>${itemData.properties}</i></div>
                <div><b style="color: green;">NATURAL 19</b></div>
                <div>${itemData.natNineteen}</div>
                <button class="melee-damage" data-str="${rollData.abilities.str.mod}" data-dmg="${itemData.damage}">Roll Damage</button>
                </html>`
              });
            } else if (dieNum == 1){
              disad.toMessage({
                speaker: ChatMessage.getSpeaker(),
                flavor: `<html>
                <div style="min-height:50px;"><img style="width:50px; height:50px; float:left; margin-right:5px; margin-top:3px;" src="${item.img}"> <h3><b>${actorData.name}</b> attacks with their <b>${item.name}</b></h3>
                <hr>
                <i>${itemData.properties}</i></div>
                <div><b style="color: red;">FUMBLE</b></div>
                </html>`
              });
            } else {
              disad.toMessage({
                speaker: ChatMessage.getSpeaker(),
                flavor: `<html>
                <div style="min-height:50px;"><img style="width:50px; height:50px; float:left; margin-right:5px; margin-top:3px;" src="${item.img}"> <h3><b>${actorData.name}</b> attacks with their <b>${item.name}</b></h3>
                <hr>
                <i>${itemData.properties}</i></div>
                <button class="melee-damage" data-str="${rollData.abilities.str.mod}" data-dmg="${itemData.damage}">Roll Damage</button>
                </html>`
              });
            }
          }

          async function meleeNorm(html) {
            let sitMod = html.find("input#sitMod").val();
            let norm = await new Roll("1d20 + @rollData.abilities.str.mod + @rollData.attributes.attackBonus.value + @sitMod", {rollData, sitMod}).evaluate({async: true});
            let dieNum = norm.result[0] + norm.result[1];

            if (dieNum >= crit && dieNum != 19){
              // Get Max Damage for Crit
              let dmg = await new Roll("@damage", rollData).evaluate({async: true});
              let maxDmg = (dmg.dice[0].faces * dmg.dice[0].number) + rollData.abilities.str.mod + lvl;

              norm.toMessage({
                speaker: ChatMessage.getSpeaker(),
                flavor: `<html>
                <div style="min-height:50px;"><img style="width:50px; height:50px; float:left; margin-right:5px; margin-top:3px;" src="${item.img}"> <h3><b>${actorData.name}</b> attacks with their <b>${item.name}</b></h3>
                <hr>
                <i>${itemData.properties}</i></div>
                <div><b style="color: green;">CRITICAL HIT</b></div>
                </br>
                <div><b>MAX DAMAGE</b> &nbsp [[${maxDmg}]]</div>
                </html>`
              });
            } else if (dieNum >= crit && dieNum == 19){
              // Get Max Damage for Crit
              let dmg = await new Roll("@damage", rollData).evaluate({async: true});
              let maxDmg = (dmg.dice[0].faces * dmg.dice[0].number) + rollData.abilities.str.mod + lvl;

              norm.toMessage({
                speaker: ChatMessage.getSpeaker(),
                flavor: `<html>
                <div style="min-height:50px;"><img style="width:50px; height:50px; float:left; margin-right:5px; margin-top:3px;" src="${item.img}"> <h3><b>${actorData.name}</b> attacks with their <b>${item.name}</b></h3>
                <hr>
                <i>${itemData.properties}</i></div>
                <div><b style="color: green;">CRITICAL HIT</b></div>
                </br>
                <div><b>MAX DAMAGE</b> &nbsp [[${maxDmg}]]</div>
                <div><b style="color: green;">NATURAL 19</b></div>
                <div>${itemData.natNineteen}</div>
                </html>`
              });
            } else if (dieNum < crit && dieNum == 19) {
              norm.toMessage({
                speaker: ChatMessage.getSpeaker(),
                flavor: `<html>
                <div style="min-height:50px;"><img style="width:50px; height:50px; float:left; margin-right:5px; margin-top:3px;" src="${item.img}"> <h3><b>${actorData.name}</b> attacks with their <b>${item.name}</b></h3>
                <hr>
                <i>${itemData.properties}</i></div>
                <div><b style="color: green;">NATURAL 19</b></div>
                <div>${itemData.natNineteen}</div>
                <button class="melee-damage" data-str="${rollData.abilities.str.mod}" data-dmg="${itemData.damage}">Roll Damage</button>
                </html>`
              });
            } else if (dieNum == 1){
              norm.toMessage({
                speaker: ChatMessage.getSpeaker(),
                flavor: `<html>
                <div style="min-height:50px;"><img style="width:50px; height:50px; float:left; margin-right:5px; margin-top:3px;" src="${item.img}"> <h3><b>${actorData.name}</b> attacks with their <b>${item.name}</b></h3>
                <hr>
                <i>${itemData.properties}</i></div>
                <div><b style="color: red;">FUMBLE</b></div>
                </html>`
              });
            } else {
              norm.toMessage({
                speaker: ChatMessage.getSpeaker(),
                flavor: `<html>
                <div style="min-height:50px;"><img style="width:50px; height:50px; float:left; margin-right:5px; margin-top:3px;" src="${item.img}"> <h3><b>${actorData.name}</b> attacks with their <b>${item.name}</b></h3>
                <hr>
                <i>${itemData.properties}</i></div>
                <button class="melee-damage" data-str="${rollData.abilities.str.mod}" data-dmg="${itemData.damage}">Roll Damage</button>
                </html>`
              });
            }
          }

          async function meleeAdv(html) {
            let sitMod = html.find("input#sitMod").val();
            let adv = await new Roll("2d20kh + @rollData.abilities.str.mod + @rollData.attributes.attackBonus.value + @sitMod", {rollData, sitMod}).evaluate({async: true});
            let dieNum = adv.result[0] + adv.result[1];

            if (dieNum >= crit && dieNum != 19){
              // Get Max Damage for Crit
              let dmg = await new Roll("@damage", rollData).evaluate({async: true});
              let maxDmg = (dmg.dice[0].faces * dmg.dice[0].number) + rollData.abilities.str.mod + lvl;

              adv.toMessage({
                speaker: ChatMessage.getSpeaker(),
                flavor: `<html>
                <div style="min-height:50px;"><img style="width:50px; height:50px; float:left; margin-right:5px; margin-top:3px;" src="${item.img}"> <h3><b>${actorData.name}</b> attacks with their <b>${item.name}</b></h3>
                <hr>
                <i>${itemData.properties}</i></div>
                <div><b style="color: green;">CRITICAL HIT</b></div>
                </br>
                <div><b>MAX DAMAGE</b> &nbsp [[${maxDmg}]]</div>
                </html>`
              });
            } else if (dieNum >= crit && dieNum == 19){
              // Get Max Damage for Crit
              let dmg = await new Roll("@damage", rollData).evaluate({async: true});
              let maxDmg = (dmg.dice[0].faces * dmg.dice[0].number) + rollData.abilities.str.mod + lvl;

              adv.toMessage({
                speaker: ChatMessage.getSpeaker(),
                flavor: `<html>
                <div style="min-height:50px;"><img style="width:50px; height:50px; float:left; margin-right:5px; margin-top:3px;" src="${item.img}"> <h3><b>${actorData.name}</b> attacks with their <b>${item.name}</b></h3>
                <hr>
                <i>${itemData.properties}</i></div>
                <div><b style="color: green;">CRITICAL HIT</b></div>
                </br>
                <div><b>MAX DAMAGE</b> &nbsp [[${maxDmg}]]</div>
                <div><b style="color: green;">NATURAL 19</b></div>
                <div>${itemData.natNineteen}</div>
                </html>`
              });
            } else if (dieNum < crit && dieNum == 19) {
              adv.toMessage({
                speaker: ChatMessage.getSpeaker(),
                flavor: `<html>
                <div style="min-height:50px;"><img style="width:50px; height:50px; float:left; margin-right:5px; margin-top:3px;" src="${item.img}"> <h3><b>${actorData.name}</b> attacks with their <b>${item.name}</b></h3>
                <hr>
                <i>${itemData.properties}</i></div>
                <div><b style="color: green;">NATURAL 19</b></div>
                <div>${itemData.natNineteen}</div>
                <button class="melee-damage" data-str="${rollData.abilities.str.mod}" data-dmg="${itemData.damage}">Roll Damage</button>
                </html>`
              });
            } else if (dieNum == 1){
              adv.toMessage({
                speaker: ChatMessage.getSpeaker(),
                flavor: `<html>
                <div style="min-height:50px;"><img style="width:50px; height:50px; float:left; margin-right:5px; margin-top:3px;" src="${item.img}"> <h3><b>${actorData.name}</b> attacks with their <b>${item.name}</b></h3>
                <hr>
                <i>${itemData.properties}</i></div>
                <div><b style="color: red;">FUMBLE</b></div>
                </html>`
              });
            } else {
              adv.toMessage({
                speaker: ChatMessage.getSpeaker(),
                flavor: `<html>
                <div style="min-height:50px;"><img style="width:50px; height:50px; float:left; margin-right:5px; margin-top:3px;" src="${item.img}"> <h3><b>${actorData.name}</b> attacks with their <b>${item.name}</b></h3>
                <hr>
                <i>${itemData.properties}</i></div>
                <button class="melee-damage" data-str="${rollData.abilities.str.mod}" data-dmg="${itemData.damage}">Roll Damage</button>
                </html>`
              });
            }
          }
          break;
        case "ranged":
          const rangedDialog = new Dialog({
            title: "Ranged Attack Modifiers",
            content: weaponDialogContent,
            buttons: {
              button1: {
                label: "Disadvantage",
                callback: (html) => rangedDisad(html)
              },
              button2: {
                label: "Normal Attack",
                callback: (html) => rangedNorm(html)
              },
              button3: {
                label: "Advantage",
                callback: (html) => rangedAdv(html)
              }
            },
            default: "button2"
          }).render(true);

          async function rangedDisad(html) {
            let sitMod = html.find("input#sitMod").val();
            let disad = await new Roll("2d20kl + @rollData.abilities.dex.mod + @rollData.attributes.attackBonus.value + @sitMod", {rollData, sitMod}).evaluate({async: true});
            let dieNum = disad.result[0] + disad.result[1];

            if (dieNum >= crit){
              // Get Max Damage for Crit
              let dmg = await new Roll("@rollData.damage", rollData).evaluate({async: true});
              let maxDmg = (dmg.dice[0].faces * dmg.dice[0].number) + rollData.abilities.dex.mod + lvl;

              disad.toMessage({
                speaker: ChatMessage.getSpeaker(),
                flavor: `<html>
                <div style="min-height:50px;"><img style="width:50px; height:50px; float:left; margin-right:5px; margin-top:3px;" src="${item.img}"> <h3><b>${actorData.name}</b> attacks with their <b>${item.name}</b></h3>
                <hr>
                <i>${itemData.properties}</i></div>
                <div><b style="color: green;">CRITICAL HIT</b></div>
                </br>
                <div><b>MAX DAMAGE</b> &nbsp [[${maxDmg}]]</div>
                </html>`
              });
            } else if (dieNum == 1){
              disad.toMessage({
                speaker: ChatMessage.getSpeaker(),
                flavor: `<html>
                <div style="min-height:50px;"><img style="width:50px; height:50px; float:left; margin-right:5px; margin-top:3px;" src="${item.img}"> <h3><b>${actorData.name}</b> attacks with their <b>${item.name}</b></h3>
                <hr>
                <i>${itemData.properties}</i></div>
                <div><b style="color: red;">FUMBLE: REROLL AGAINST ALLY IN MELEE</b></div>
                </html>`
              });
            } else {
              disad.toMessage({
                speaker: ChatMessage.getSpeaker(),
                flavor: `<html>
                <div style="min-height:50px;"><img style="width:50px; height:50px; float:left; margin-right:5px; margin-top:3px;" src="${item.img}"> <h3><b>${actorData.name}</b> attacks with their <b>${item.name}</b></h3>
                <hr>
                <i>${itemData.properties}</i></div>
                <button class="ranged-damage" data-dex="${rollData.abilities.dex.mod}" data-dmg="${itemData.damage}">Roll Damage</button>
                </html>`
              });
            }
          }

          async function rangedNorm(html) {
            let sitMod = html.find("input#sitMod").val();
            console.log(sitMod);
            let norm = await new Roll("1d20 + @rollData.abilities.dex.mod + @rollData.attributes.attackBonus.value + @sitMod", {rollData, sitMod}).evaluate({async: true});
            let dieNum = norm.result[0] + norm.result[1];

            if (dieNum >= crit){
              // Get Max Damage for Crit
              let dmg = await new Roll("@rollData.damage", rollData).evaluate({async: true});
              let maxDmg = (dmg.dice[0].faces * dmg.dice[0].number) + rollData.abilities.dex.mod + lvl;

              norm.toMessage({
                speaker: ChatMessage.getSpeaker(),
                flavor: `<html>
                <div style="min-height:50px;"><img style="width:50px; height:50px; float:left; margin-right:5px; margin-top:3px;" src="${item.img}"> <h3><b>${actorData.name}</b> attacks with their <b>${item.name}</b></h3>
                <hr>
                <i>${itemData.properties}</i></div>
                <div><b style="color: green;">CRITICAL HIT</b></div>
                </br>
                <div><b>MAX DAMAGE</b> &nbsp [[${maxDmg}]]</div>
                </html>`
              });
            } else if (dieNum == 1){
              norm.toMessage({
                speaker: ChatMessage.getSpeaker(),
                flavor: `<html>
                <div style="min-height:50px;"><img style="width:50px; height:50px; float:left; margin-right:5px; margin-top:3px;" src="${item.img}"> <h3><b>${actorData.name}</b> attacks with their <b>${item.name}</b></h3>
                <hr>
                <i>${itemData.properties}</i></div>
                <div><b style="color: red;">FUMBLE: REROLL AGAINST ALLY IN MELEE</b></div>
                </html>`
              });
            } else {
              norm.toMessage({
                speaker: ChatMessage.getSpeaker(),
                flavor: `<html>
                <div style="min-height:50px;"><img style="width:50px; height:50px; float:left; margin-right:5px; margin-top:3px;" src="${item.img}"> <h3><b>${actorData.name}</b> attacks with their <b>${item.name}</b></h3>
                <hr>
                <i>${itemData.properties}</i></div>
                <button class="ranged-damage" data-dex="${rollData.abilities.dex.mod}" data-dmg="${itemData.damage}">Roll Damage</button>
                </html>`
              });
            }
          }

          async function rangedAdv(html) {
            let sitMod = html.find("input#sitMod").val();
            let adv = await new Roll("2d20kh + @rollData.abilities.dex.mod + @rollData.attributes.attackBonus.value + @sitMod", {rollData, sitMod}).evaluate({async: true});
            let dieNum = adv.result[0] + adv.result[1];

            if (dieNum >= crit){
              // Get Max Damage for Crit
              let dmg = await new Roll("@rollData.damage", rollData).evaluate({async: true});
              let maxDmg = (dmg.dice[0].faces * dmg.dice[0].number) + rollData.abilities.dex.mod + lvl;

              adv.toMessage({
                speaker: ChatMessage.getSpeaker(),
                flavor: `<html>
                <div style="min-height:50px;"><img style="width:50px; height:50px; float:left; margin-right:5px; margin-top:3px;" src="${item.img}"> <h3><b>${actorData.name}</b> attacks with their <b>${item.name}</b></h3>
                <hr>
                <i>${itemData.properties}</i></div>
                <div><b style="color: green;">CRITICAL HIT</b></div>
                </br>
                <div><b>MAX DAMAGE</b> &nbsp [[${maxDmg}]]</div>
                </html>`
              });
            } else if (dieNum == 1){
              adv.toMessage({
                speaker: ChatMessage.getSpeaker(),
                flavor: `<html>
                <div style="min-height:50px;"><img style="width:50px; height:50px; float:left; margin-right:5px; margin-top:3px;" src="${item.img}"> <h3><b>${actorData.name}</b> attacks with their <b>${item.name}</b></h3>
                <hr>
                <i>${itemData.properties}</i></div>
                <div><b style="color: red;">FUMBLE: REROLL AGAINST ALLY IN MELEE</b></div>
                </html>`
              });
            } else {
              adv.toMessage({
                speaker: ChatMessage.getSpeaker(),
                flavor: `<html>
                <div style="min-height:50px;"><img style="width:50px; height:50px; float:left; margin-right:5px; margin-top:3px;" src="${item.img}"> <h3><b>${actorData.name}</b> attacks with their <b>${item.name}</b></h3>
                <hr>
                <i>${itemData.properties}</i></div>
                <button class="ranged-damage" data-dex="${rollData.abilities.dex.mod}" data-dmg="${itemData.damage}">Roll Damage</button>
                </html>`
              });
            }
          }
          break;
        default: ui.notifications.warn("Please set your weapon's ~Weapon Type~ to Melee or Ranged.")
      }
    });

    // Delete Inventory Item
    html.find('.item-delete').click(ev => {
      const li = $(ev.currentTarget).parents(".item");
      const item = this.actor.items.get(li.data("itemId"));
      if ( item ) return item.delete();
      // this.actor.deleteEmbeddedDocuments(li.data("itemId"));
      li.slideUp(200, () => this.render(false));
    });

    // Rollable abilities.
    html.find('.rollable').click(this._onRoll.bind(this));
    html.find('.rollable-luck').click(this._onRollLuck.bind(this));
    html.find('.npc-luck').click(this._onNpcLuck.bind(this));
    html.find('.rollable-ddm').click(this._onRollDDM.bind(this));

    // Drag events for macros.
    if (this.actor.isOwner) {
      let handler = ev => this._onDragItemStart(ev);
      html.find('li.item').each((i, li) => {
        if (li.classList.contains("inventory-header")) return;
        li.setAttribute("draggable", true);
        li.addEventListener("dragstart", handler, false);
      });
    }

    // Rollable NPC HD and NoAppear
    html.find('.npc-rollable').click(this._npcRoll.bind(this));
  }

  /**
   * Handle creating a new Owned Item for the actor using initial data defined in the HTML dataset
   * @param {Event} event   The originating click event
   * @private
   */
  _onItemCreate(event) {
    event.preventDefault();
    const header = event.currentTarget;
    // Get the type of item to create.
    const type = header.dataset.type;
    // Grab any data associated with this control.
    const data = duplicate(header.dataset);
    // Initialize a default name.
    const name = `New ${type.capitalize()}`;
    // Prepare the item object.
    const itemData = {
      name: name,
      type: type,
      data: data
    };
    // Remove the type from the dataset since it's in the itemData.type prop.
    delete itemData.data["type"];

    // Finally, create the item!
    return this.actor.createEmbeddedDocuments("Item", [itemData]);
  }

  getLuckModifer(){
    const modifier = this.element.find('#luck-modifier')[0].value;
    if (!modifier){
      return 0;
    }
    return this.actor.system.abilities[modifier].mod;
  }
  async roll(data) {
    const result = await lfgRoll(data);
    result.roll
      .toMessage({ 
        speaker: ChatMessage.getSpeaker({ actor: this.actor }), 
        flavor: successLevelMessage(result) });
    return result;
  }
  magicRoll(data) {
    const result = ddmRoll(data);
    result.roll
      .toMessage({
        speaker: ChatMessage.getSpeaker({ actor: this.actor }),
        flavor: ddmMessage(result)
      });
    return result;
  }
  async incrementDDM(){
    if (this.actor.system.effects.ddm.value <= 0){
      return false;
    }
    try{
      await this.actor.update({[`data.effects.ddm.value`]: this.actor.system.effects.ddm.value + 1}); // Updates one EmbeddedEntity
    } catch(e){
      console.log(e)
    }
  }
  async resetDDM(){
    if (this.actor.system.effects.ddm.value <= 0){
      return false;
    }
    try{
      await this.actor.update({[`data.effects.ddm.value`]: 1}); // Updates one EmbeddedEntity
    } catch(e) {
      console.log(e)
    }
  }
  async decrementLuck(){
    if (this.actor.system.resources.luck.value <= 0){
      return false;
    }
    try{
      await this.actor.update({[`data.resources.luck.value`]: this.actor.system.resources.luck.value - 1}); // Updates one EmbeddedEntity
    } catch(e){
      console.log(e)
    }
  }
  /**
   * Handle clickable rolls.
   * @param {Event} event   The originating click event
   * @private
   */

  async _onRollLuck(event) {
    event.preventDefault();
    const result = await this.roll({...event.currentTarget.dataset,
      mod: this.getLuckModifer(),
      adv: event.shiftKey,
      disad: event.altKey
    });
    console.log(result);
    if (result.roll.total <= result.target ){
      this.decrementLuck();
    }
  }
  _onNpcLuck(event) {
    event.preventDefault();
    const result = this.roll({...event.currentTarget.dataset,
      mod: this.getLuckModifer(),
      adv: event.shiftKey,
      disad: event.altKey
    });
  }
  _onRoll(event) {
    event.preventDefault();
    this.roll({...event.currentTarget.dataset,
      adv: event.shiftKey,
      disad: event.altKey
    });
  }

  _onRollDDM(event) {
    event.preventDefault();
    const result = this.magicRoll(event.currentTarget.dataset);
    if (result.roll.total > result.target ){
      this.incrementDDM();
    } else {
      this.resetDDM();
    }
  }

  async _onToggleSkillProficiency(event) {
    event.preventDefault();
    const name = event.currentTarget.dataset.skill
    try{
      await this.actor.update({[`data.skills.${name}.skilled`]: !this.actor.system.skills[name].skilled}); // Updates one EmbeddedEntity
    } catch(e){
      console.log(e)
    }
  }

  async _npcRoll(event) {
    event.preventDefault();
    const element = event.currentTarget;
    const dataset = element.dataset;

    if (dataset.roll) {
      let label = dataset.label ? `${dataset.label}` : '';
      let roll = await new Roll(dataset.roll, this.actor.system).evaluate();
      roll.toMessage({
        speaker: ChatMessage.getSpeaker({ actor: this.actor }),
        flavor: label
      });
    }
  }
}
