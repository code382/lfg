/**
 * Extend the basic ActorSheet with some very simple modifications
 * @extends {ActorSheet}
 */
import { lfgRoll, ddmRoll, ddmMessage, successLevelMessage } from "../lfgutils.js";
import { weaponDialogContent } from "../lfg-dialog.js"

export class LFGActorSheetTest extends ActorSheet {
  /** @override */
  static get defaultOptions() {
    return foundry.utils.mergeObject(super.defaultOptions, {
      classes: ["lfg", "sheet", "actor"],
      width: 750,
      height: 900,
      tabs: [{ navSelector: ".sheet-tabs", contentSelector: ".sheet-body2", initial: "items" }]
    });
  }

  /** @override */
get template() {
  if (this.actor.type == 'character') {
    return `systems/lfg/templates/actor/test-sheet.html`;
  } else if (this.actor.type == 'npc') {
    return `systems/lfg/templates/actor/npc-sheet.html`;
  }
}

  /* -------------------------------------------- */
  
  /** @override */
  async getData() {
    const data = super.getData();
    data.dtypes = ["String", "Number", "Boolean"];
    //v10 Enriched Editor
    data.actor.enrichedBiography = await TextEditor.enrichHTML(data.actor.system.biography, {async: true});

    // Prepare items.
    if (this.actor.type == 'character') {
      this._prepareCharacterItems(data);
    }
    if (this.actor.type == 'npc') {
      this._prepareNpcData(data);
    }

    return data;
  }

  /**
   * Organize and classify Items for Character sheets.
   *
   * @param {Object} actorData The actor to prepare.
   *
   * @return {undefined}
   */
  async _prepareCharacterItems(sheetData) {
    const actorData = sheetData.actor;


    // Initialize containers.
    const gear = [];
    const weapons = [];
    const features = [];
    const skills = [];
    const spells = {
      1: [],
      2: [],
      3: [],
      4: [],
      5: [],
      6: []
    };
    const injSetbacks = [];
    const madness = [];
    const darkMagic = [];

    // Iterate through items, allocating to containers
    // let totalWeight = 0;
    for (let i of sheetData.items) {
      let item = i.system;
      i.img = i.img || DEFAULT_TOKEN;
      // Append to gear.
      if (i.type === 'item') {
        gear.push(i);
      }
      // Append to weapons.
      if (i.type === 'weapon') {
        weapons.push(i);
      }
      // Append to features.
      else if (i.type === 'feature') {
        features.push(i);
      }
      // Append to skills.
      else if (i.type === 'skill') {
        skills.push(i);
      }
      // Append to spells.
      else if (i.type === 'spell') {
        if (i.system.spellLevel != undefined) {
          spells[i.system.spellLevel].push(i);
        }
      }
       // Append to injuries and setbacks.
      else if (i.type === 'injSetback') {
        injSetbacks.push(i);
      }
      else if (i.type === 'madness') {
        madness.push(i);
      }
      else if (i.type === 'darkMagic') {
        darkMagic.push(i);
      }
    }

    // Assign and return
    actorData.gear = gear;
    actorData.weapons = weapons;
    actorData.features = features;
    actorData.skills = skills;
    actorData.spells = spells;
    actorData.injSetbacks = injSetbacks;
    actorData.madness = madness;
    actorData.darkMagic = darkMagic;
  }

  async _prepareNpcData(sheetData) {
    const actorData = sheetData.actor;

    //v10 Enriched Editor
    actorData.enrichedDescription = await TextEditor.enrichHTML(actorData.system.npcStats.description, {async: true});
  }

  /* -------------------------------------------- */

  /** @override */
  activateListeners(html) {
    super.activateListeners(html);

    // Everything below here is only needed if the sheet is editable
    if (!this.options.editable) return;

    // html.find('.skill-proficient').on("click contextmenu", this._onToggleSkillProficiency.bind(this));

    // Add Inventory Item
    html.find('.item-create').click(this._onItemCreate.bind(this));

    // Update Inventory Item
    html.find('.item-edit').click(ev => {
      const li = $(ev.currentTarget).parents(".item");
      const item = this.actor.items.get(li.data("itemId"));
      item.sheet.render(true);
    });

    // Adding Quantity to Item
    html.find('.item-add').click(ev => {
      const li = $(ev.currentTarget).parents(".item");
      const item = this.actor.items.get(li.data("itemId"));
      const itemData = item.system;

      console.log(item.type);

      if (item.type == "weapon") {
        if (!itemData.ammo) {
          item.update({ 'system.ammo': 1 });
        } else {
          item.update({ 'system.ammo': itemData.ammo + 1 });
        }
      }

      if (item.type == "item") {
        if (!itemData.quantity) {
          item.update({ 'system.quantity': 1 });
        } else {
          item.update({ 'system.quantity': itemData.quantity + 1 });
        }
      }

      if (item.type == "feature") {
        if (!itemData.uses.value) {
          item.update({ 'system.uses.value': 1 });
        } else {
          item.update({ 'system.uses.value': itemData.uses.value + 1 });
        }
      }
    });

    // Subtracting Quantity from Item
    html.find('.item-subtract').click(ev => {
      const li = $(ev.currentTarget).parents(".item");
      const item = this.actor.items.get(li.data("itemId"));
      const itemData = item.system;

      console.log(item.type);

      if (item.type == "weapon") {
         item.update({ 'system.ammo': itemData.ammo - 1 });
        }

      if (item.type == "item") {
        item.update({ 'system.quantity': itemData.quantity - 1});
      }

      if (item.type == "feature") {
        item.update({ 'system.uses.value': itemData.uses.value - 1});
      }
    });

    // Send Item info to Chat
    html.find('.item-chat').click(ev => {
      const li = $(ev.currentTarget).parents(".item");
      const item = this.actor.items.get(li.data("itemId"));
      const itemData = item.system;

      console.log(itemData.properties);

      ChatMessage.create({
        content: `<html>
                  <div style="min-height:50px;"><img style="width:50px; height:50px; float:left; margin-right:5px; margin-top:3px;" src="${item.img}"> <h1 style="border-bottom:none;">${item.name}</h1></div>
                  <hr>
                  <div style="clear:left;">${itemData.properties}</div>
                  </html>`
      });
    });

    // Work Around for Spells to Chat
    html.find('.spell-chat').click(ev => {
      const li = $(ev.currentTarget).parents(".item");
      const item = this.actor.items.get(li.data("itemId"));
      const itemData = item.system;

      console.log(itemData.properties);

      ChatMessage.create({
        content: `<html>
                  <div style="min-height:50px;"><img style="width:50px; height:50px; float:left; margin-right:5px; margin-top:3px;" src="${item.img}"> <h1 style="border-bottom:none;">${item.name}</h1></div>
                  <hr>
                  <div style="clear:left;"><h4 style="font-size: large;">Range: ${itemData.spellRange}, Duration: ${itemData.spellDuration}, Save: ${itemData.spellSave}</h4></div>
                  <hr>
                  <div>${itemData.properties}</div>
                  </html>`
      });
    });

    // Weapon Attacks to Chat
    this.element.find('.weapon-chat').click(ev => {

      // Getting Item and Actor Data
      const li = this.element.find(ev.currentTarget).parents(".item");
      const item = this.actor.items.get(li.data("itemId"));
      const itemData = item.system;
      const rollData = this.actor.getRollData();
      const actorData = this.actor.system;
      const actorName = this.actor.name;
      foundry.utils.mergeObject(rollData, itemData);

      // Testing for Correct Data
      console.log(rollData);

      // Creating and Adjusting Additional Relevant Modifiers
      let attackType = itemData.type.toLowerCase();
      let crit;
      let lvl = Math.ceil(actorData.about.level.value / 2);

      // Assigning the Crit Value
      switch (actorData.attributes.critRange.value) {
        case "20":
          crit = 20;
          break;
        case "19-20":
          crit = 19;
          break;
        case "18-20":
          crit = 18;
      }

      switch (attackType) {
        case "melee":
          const meleeDialog = new Dialog({
            title: "Melee Attack Modifiers",
            content: weaponDialogContent,
            buttons: {
              button1: {
                label: "Disadvantage",
                callback: (html) => meleeDisad(html)
              },
              button2: {
                label: "Normal Attack",
                callback: (html) => meleeNorm(html)
              },
              button3: {
                label: "Advantage",
                callback: (html) => meleeAdv(html)
              }
            },
            default: "button2"
          }).render(true);

          async function meleeDisad(html) {
            let sitMod = html.find("input#sitMod").val();
            let disad = await new Roll("2d20kl + @rollData.abilities.str.mod + @rollData.attributes.attackBonus.value + @sitMod", {rollData, sitMod}).evaluate();
            let dieNum = disad.result[0] + disad.result[1];

            if (dieNum >= crit && dieNum != 19){
              // Get Max Damage for Crit
              let dmg = new Roll("@damage", rollData);
              await dmg.evaluate();
              let maxDmg = (dmg.dice[0].faces * dmg.dice[0].number) + rollData.abilities.str.mod + lvl;

              disad.toMessage({
                speaker: ChatMessage.getSpeaker(),
                flavor: `<html>
                <div style="min-height:50px;"><img style="width:50px; height:50px; float:left; margin-right:5px; margin-top:3px;" src="${item.img}"> <h4 style="font-size: large;"><b>${actorName}</b> attacks with their <b>${item.name}</b></h4>
                <hr>
                <i>${itemData.properties}</i></div>
                <div><b style="color: green;">CRITICAL HIT</b></div>
                </br>
                <div><b>MAX DAMAGE</b> &nbsp [[${maxDmg}]]</div>
                </html>`
              });
            } else if (dieNum >= crit && dieNum == 19){
              // Get Max Damage for Crit
              let dmg = new Roll("@damage", rollData);
              await dmg.evaluate();
              let maxDmg = (dmg.dice[0].faces * dmg.dice[0].number) + rollData.abilities.str.mod + lvl;

              disad.toMessage({
                speaker: ChatMessage.getSpeaker(),
                flavor: `<html>
                <div style="min-height:50px;"><img style="width:50px; height:50px; float:left; margin-right:5px; margin-top:3px;" src="${item.img}"> <h4 style="font-size: large;"><b>${actorName}</b> attacks with their <b>${item.name}</b></h4>
                <hr>
                <i>${itemData.properties}</i></div>
                <div><b style="color: green;">CRITICAL HIT</b></div>
                </br>
                <div><b>MAX DAMAGE</b> &nbsp [[${maxDmg}]]</div>
                <div><b style="color: green;">NATURAL 19</b></div>
                <div>${itemData.natNineteen}</div>
                </html>`
              });
            } else if (dieNum < crit && dieNum == 19) {
              disad.toMessage({
                speaker: ChatMessage.getSpeaker(),
                flavor: `<html>
                <div style="min-height:50px;"><img style="width:50px; height:50px; float:left; margin-right:5px; margin-top:3px;" src="${item.img}"> <h4 style="font-size: large;"><b>${actorName}</b> attacks with their <b>${item.name}</b></h4>
                <hr>
                <i>${itemData.properties}</i></div>
                <div><b style="color: green;">NATURAL 19</b></div>
                <div>${itemData.natNineteen}</div>
                <button class="melee-damage" data-str="${rollData.abilities.str.mod}" data-dmg="${itemData.damage}">Roll Damage</button>
                </html>`
              });
            } else if (dieNum == 1){
              disad.toMessage({
                speaker: ChatMessage.getSpeaker(),
                flavor: `<html>
                <div style="min-height:50px;"><img style="width:50px; height:50px; float:left; margin-right:5px; margin-top:3px;" src="${item.img}"> <h4 style="font-size: large;"><b>${actorName}</b> attacks with their <b>${item.name}</b></h4>
                <hr>
                <i>${itemData.properties}</i></div>
                <div><b style="color: red;">FUMBLE</b></div>
                </html>`
              });
            } else {
              disad.toMessage({
                speaker: ChatMessage.getSpeaker(),
                flavor: `<html>
                <div style="min-height:50px;"><img style="width:50px; height:50px; float:left; margin-right:5px; margin-top:3px;" src="${item.img}"> <h4 style="font-size: large;"><b>${actorName}</b> attacks with their <b>${item.name}</b></h4>
                <hr>
                <i>${itemData.properties}</i></div>
                <button class="melee-damage" data-str="${rollData.abilities.str.mod}" data-dmg="${itemData.damage}">Roll Damage</button>
                </html>`
              });
            }
          }

          async function meleeNorm(html) {
            let sitMod = html.find("input#sitMod").val();
            let norm = await new Roll("1d20 + @rollData.abilities.str.mod + @rollData.attributes.attackBonus.value + @sitMod", {rollData, sitMod}).evaluate();
            let dieNum = norm.result[0] + norm.result[1];

            if (dieNum >= crit && dieNum != 19){
              // Get Max Damage for Crit
              let dmg = new Roll("@damage", rollData);
              await dmg.evaluate();
              let maxDmg = (dmg.dice[0].faces * dmg.dice[0].number) + rollData.abilities.str.mod + lvl;

              norm.toMessage({
                speaker: ChatMessage.getSpeaker(),
                flavor: `<html>
                <div style="min-height:50px;"><img style="width:50px; height:50px; float:left; margin-right:5px; margin-top:3px;" src="${item.img}"> <h4 style="font-size: large;"><b>${actorName}</b> attacks with their <b>${item.name}</b></h4>
                <hr>
                <i>${itemData.properties}</i></div>
                <div><b style="color: green;">CRITICAL HIT</b></div>
                </br>
                <div><b>MAX DAMAGE</b> &nbsp [[${maxDmg}]]</div>
                </html>`
              });
            } else if (dieNum >= crit && dieNum == 19){
              // Get Max Damage for Crit
              let dmg = new Roll("@damage", rollData);
              await dmg.evaluate();
              let maxDmg = (dmg.dice[0].faces * dmg.dice[0].number) + rollData.abilities.str.mod + lvl;

              norm.toMessage({
                speaker: ChatMessage.getSpeaker(),
                flavor: `<html>
                <div style="min-height:50px;"><img style="width:50px; height:50px; float:left; margin-right:5px; margin-top:3px;" src="${item.img}"> <h4 style="font-size: large;"><b>${actorName}</b> attacks with their <b>${item.name}</b></h4>
                <hr>
                <i>${itemData.properties}</i></div>
                <div><b style="color: green;">CRITICAL HIT</b></div>
                </br>
                <div><b>MAX DAMAGE</b> &nbsp [[${maxDmg}]]</div>
                <div><b style="color: green;">NATURAL 19</b></div>
                <div>${itemData.natNineteen}</div>
                </html>`
              });
            } else if (dieNum < crit && dieNum == 19) {
              norm.toMessage({
                speaker: ChatMessage.getSpeaker(),
                flavor: `<html>
                <div style="min-height:50px;"><img style="width:50px; height:50px; float:left; margin-right:5px; margin-top:3px;" src="${item.img}"> <h4 style="font-size: large;"><b>${actorName}</b> attacks with their <b>${item.name}</b></h4>
                <hr>
                <i>${itemData.properties}</i></div>
                <div><b style="color: green;">NATURAL 19</b></div>
                <div>${itemData.natNineteen}</div>
                <button class="melee-damage" data-str="${rollData.abilities.str.mod}" data-dmg="${itemData.damage}">Roll Damage</button>
                </html>`
              });
            } else if (dieNum == 1){
              norm.toMessage({
                speaker: ChatMessage.getSpeaker(),
                flavor: `<html>
                <div style="min-height:50px;"><img style="width:50px; height:50px; float:left; margin-right:5px; margin-top:3px;" src="${item.img}"> <h4 style="font-size: large;"><b>${actorName}</b> attacks with their <b>${item.name}</b></h4>
                <hr>
                <i>${itemData.properties}</i></div>
                <div><b style="color: red;">FUMBLE</b></div>
                </html>`
              });
            } else {
              norm.toMessage({
                speaker: ChatMessage.getSpeaker(),
                flavor: `<html>
                <div style="min-height:50px;"><img style="width:50px; height:50px; float:left; margin-right:5px; margin-top:3px;" src="${item.img}"> <h4 style="font-size: large;"><b>${actorName}</b> attacks with their <b>${item.name}</b></h4>
                <hr>
                <i>${itemData.properties}</i></div>
                <button class="melee-damage" data-str="${rollData.abilities.str.mod}" data-dmg="${itemData.damage}">Roll Damage</button>
                </html>`
              });
            }
          }

          async function meleeAdv(html) {
            let sitMod = html.find("input#sitMod").val();
            let adv = await new Roll("2d20kh + @rollData.abilities.str.mod + @rollData.attributes.attackBonus.value + @sitMod", {rollData, sitMod}).evaluate();
            let dieNum = adv.result[0] + adv.result[1];

            if (dieNum >= crit && dieNum != 19){
              // Get Max Damage for Crit
              let dmg = new Roll("@damage", rollData);
              await dmg.evaluate();
              let maxDmg = (dmg.dice[0].faces * dmg.dice[0].number) + rollData.abilities.str.mod + lvl;

              adv.toMessage({
                speaker: ChatMessage.getSpeaker(),
                flavor: `<html>
                <div style="min-height:50px;"><img style="width:50px; height:50px; float:left; margin-right:5px; margin-top:3px;" src="${item.img}"> <h4 style="font-size: large;"><b>${actorName}</b> attacks with their <b>${item.name}</b></h4>
                <hr>
                <i>${itemData.properties}</i></div>
                <div><b style="color: green;">CRITICAL HIT</b></div>
                </br>
                <div><b>MAX DAMAGE</b> &nbsp [[${maxDmg}]]</div>
                </html>`
              });
            } else if (dieNum >= crit && dieNum == 19){
              // Get Max Damage for Crit
              let dmg = new Roll("@damage", rollData);
              await dmg.evaluate();
              let maxDmg = (dmg.dice[0].faces * dmg.dice[0].number) + rollData.abilities.str.mod + lvl;

              adv.toMessage({
                speaker: ChatMessage.getSpeaker(),
                flavor: `<html>
                <div style="min-height:50px;"><img style="width:50px; height:50px; float:left; margin-right:5px; margin-top:3px;" src="${item.img}"> <h4 style="font-size: large;"><b>${actorName}</b> attacks with their <b>${item.name}</b></h4>
                <hr>
                <i>${itemData.properties}</i></div>
                <div><b style="color: green;">CRITICAL HIT</b></div>
                </br>
                <div><b>MAX DAMAGE</b> &nbsp [[${maxDmg}]]</div>
                <div><b style="color: green;">NATURAL 19</b></div>
                <div>${itemData.natNineteen}</div>
                </html>`
              });
            } else if (dieNum < crit && dieNum == 19) {
              adv.toMessage({
                speaker: ChatMessage.getSpeaker(),
                flavor: `<html>
                <div style="min-height:50px;"><img style="width:50px; height:50px; float:left; margin-right:5px; margin-top:3px;" src="${item.img}"> <h4 style="font-size: large;"><b>${actorName}</b> attacks with their <b>${item.name}</b></h4>
                <hr>
                <i>${itemData.properties}</i></div>
                <div><b style="color: green;">NATURAL 19</b></div>
                <div>${itemData.natNineteen}</div>
                <button class="melee-damage" data-str="${rollData.abilities.str.mod}" data-dmg="${itemData.damage}">Roll Damage</button>
                </html>`
              });
            } else if (dieNum == 1){
              adv.toMessage({
                speaker: ChatMessage.getSpeaker(),
                flavor: `<html>
                <div style="min-height:50px;"><img style="width:50px; height:50px; float:left; margin-right:5px; margin-top:3px;" src="${item.img}"> <h4 style="font-size: large;"><b>${actorName}</b> attacks with their <b>${item.name}</b></h4>
                <hr>
                <i>${itemData.properties}</i></div>
                <div><b style="color: red;">FUMBLE</b></div>
                </html>`
              });
            } else {
              adv.toMessage({
                speaker: ChatMessage.getSpeaker(),
                flavor: `<html>
                <div style="min-height:50px;"><img style="width:50px; height:50px; float:left; margin-right:5px; margin-top:3px;" src="${item.img}"> <h4 style="font-size: large;"><b>${actorName}</b> attacks with their <b>${item.name}</b></h4>
                <hr>
                <i>${itemData.properties}</i></div>
                <button class="melee-damage" data-str="${rollData.abilities.str.mod}" data-dmg="${itemData.damage}">Roll Damage</button>
                </html>`
              });
            }
          }
          break;
        case "ranged":
          const rangedDialog = new Dialog({
            title: "Ranged Attack Modifiers",
            content: weaponDialogContent,
            buttons: {
              button1: {
                label: "Disadvantage",
                callback: (html) => rangedDisad(html)
              },
              button2: {
                label: "Normal Attack",
                callback: (html) => rangedNorm(html)
              },
              button3: {
                label: "Advantage",
                callback: (html) => rangedAdv(html)
              }
            },
            default: "button2"
          }).render(true);

          async function rangedDisad(html) {
            let sitMod = html.find("input#sitMod").val();

            const rangedPerc = game.settings.get("lfg", "percRanged");
            let disadRollFormula = "2d20kl + @rollData.abilities.dex.mod + @rollData.attributes.attackBonus.value + @sitMod";
            let dataDex = rollData.abilities.dex.mod;
            if (rangedPerc == "perc") {
              disadRollFormula = "2d20kl + @rollData.abilities.perc.mod + @rollData.attributes.attackBonus.value + @sitMod";
              dataDex = rollData.abilities.perc.mod;
            }

            let disad = await new Roll(disadRollFormula, {rollData, sitMod}).evaluate();
            let dieNum = disad.result[0] + disad.result[1];

            if (dieNum >= crit){
              // Get Max Damage for Crit
              let dmg = new Roll("@damage", rollData);
              await dmg.evaluate;
              let maxDmg = (dmg.dice[0].faces * dmg.dice[0].number) + rollData.abilities.dex.mod + lvl;

              disad.toMessage({
                speaker: ChatMessage.getSpeaker(),
                flavor: `<html>
                <div style="min-height:50px;"><img style="width:50px; height:50px; float:left; margin-right:5px; margin-top:3px;" src="${item.img}"> <h4 style="font-size: large;"><b>${actorName}</b> attacks with their <b>${item.name}</b></h4>
                <hr>
                <i>${itemData.properties}</i></div>
                <div><b style="color: green;">CRITICAL HIT</b></div>
                </br>
                <div><b>MAX DAMAGE</b> &nbsp [[${maxDmg}]]</div>
                </html>`
              });
            } else if (dieNum == 1){
              disad.toMessage({
                speaker: ChatMessage.getSpeaker(),
                flavor: `<html>
                <div style="min-height:50px;"><img style="width:50px; height:50px; float:left; margin-right:5px; margin-top:3px;" src="${item.img}"> <h4 style="font-size: large;"><b>${actorName}</b> attacks with their <b>${item.name}</b></h4>
                <hr>
                <i>${itemData.properties}</i></div>
                <div><b style="color: red;">FUMBLE: REROLL AGAINST ALLY IN MELEE</b></div>
                </html>`
              });
            } else {
              disad.toMessage({
                speaker: ChatMessage.getSpeaker(),
                flavor: `<html>
                <div style="min-height:50px;"><img style="width:50px; height:50px; float:left; margin-right:5px; margin-top:3px;" src="${item.img}"> <h4 style="font-size: large;"><b>${actorName}</b> attacks with their <b>${item.name}</b></h4>
                <hr>
                <i>${itemData.properties}</i></div>
                <button class="ranged-damage" data-dex="${dataDex}" data-dmg="${itemData.damage}">Roll Damage</button>
                </html>`
              });
            }
          }

          async function rangedNorm(html) {
            let sitMod = html.find("input#sitMod").val();
            console.log(sitMod);

            const rangedPerc = game.settings.get("lfg", "percRanged");
            console.log(rangedPerc);
            let normRollFormula = "1d20 + @rollData.abilities.dex.mod + @rollData.attributes.attackBonus.value + @sitMod";
            let dataDex = rollData.abilities.dex.mod;
            if (rangedPerc == "perc") {
              normRollFormula = "1d20 + @rollData.abilities.perc.mod + @rollData.attributes.attackBonus.value + @sitMod";
              dataDex = rollData.abilities.perc.mod;
            }

            let norm = await new Roll(normRollFormula, {rollData, sitMod}).evaluate();
            let dieNum = norm.result[0] + norm.result[1];
            console.log("DAMAGE DIE: " + rollData.damage);

            if (dieNum >= crit){
              // Get Max Damage for Crit
              let dmg = new Roll("@damage", rollData);
              await dmg.evaluate;
              let maxDmg = (dmg.dice[0].faces * dmg.dice[0].number) + rollData.abilities.dex.mod + lvl;

              norm.toMessage({
                speaker: ChatMessage.getSpeaker(),
                flavor: `<html>
                <div style="min-height:50px;"><img style="width:50px; height:50px; float:left; margin-right:5px; margin-top:3px;" src="${item.img}"> <h4 style="font-size: large;"><b>${actorName}</b> attacks with their <b>${item.name}</b></h4>
                <hr>
                <i>${itemData.properties}</i></div>
                <div><b style="color: green;">CRITICAL HIT</b></div>
                </br>
                <div><b>MAX DAMAGE</b> &nbsp [[${maxDmg}]]</div>
                </html>`
              });
            } else if (dieNum == 1){
              norm.toMessage({
                speaker: ChatMessage.getSpeaker(),
                flavor: `<html>
                <div style="min-height:50px;"><img style="width:50px; height:50px; float:left; margin-right:5px; margin-top:3px;" src="${item.img}"> <h4 style="font-size: large;"><b>${actorName}</b> attacks with their <b>${item.name}</b></h4>
                <hr>
                <i>${itemData.properties}</i></div>
                <div><b style="color: red;">FUMBLE: REROLL AGAINST ALLY IN MELEE</b></div>
                </html>`
              });
            } else {
              norm.toMessage({
                speaker: ChatMessage.getSpeaker(),
                flavor: `<html>
                <div style="min-height:50px;"><img style="width:50px; height:50px; float:left; margin-right:5px; margin-top:3px;" src="${item.img}"> <h4 style="font-size: large;"><b>${actorName}</b> attacks with their <b>${item.name}</b></h4>
                <hr>
                <i>${itemData.properties}</i></div>
                <button class="ranged-damage" data-dex="${dataDex}" data-dmg="${itemData.damage}">Roll Damage</button>
                </html>`
              });
            }
          }

          async function rangedAdv(html) {
            let sitMod = html.find("input#sitMod").val();
            const rangedPerc = game.settings.get("lfg", "percRanged");
            let advRollFormula = "2d20kh + @rollData.abilities.dex.mod + @rollData.attributes.attackBonus.value + @sitMod";
            let dataDex = rollData.abilities.dex.mod;
            if (rangedPerc == "perc") {
              advRollFormula = "2d20kh + @rollData.abilities.perc.mod + @rollData.attributes.attackBonus.value + @sitMod";
              dataDex = rollData.abilities.perc.mod;
            }

            let adv = await new Roll(advRollFormula, {rollData, sitMod}).evaluate();
            let dieNum = adv.result[0] + adv.result[1];

            if (dieNum >= crit){
              // Get Max Damage for Crit
              let dmg = new Roll("@damage", rollData);
              await dmg.evaluate;
              let maxDmg = (dmg.dice[0].faces * dmg.dice[0].number) + rollData.abilities.dex.mod + lvl;

              adv.toMessage({
                speaker: ChatMessage.getSpeaker(),
                flavor: `<html>
                <div style="min-height:50px;"><img style="width:50px; height:50px; float:left; margin-right:5px; margin-top:3px;" src="${item.img}"> <h4 style="font-size: large;"><b>${actorName}</b> attacks with their <b>${item.name}</b></h4>
                <hr>
                <i>${itemData.properties}</i></div>
                <div><b style="color: green;">CRITICAL HIT</b></div>
                </br>
                <div><b>MAX DAMAGE</b> &nbsp [[${maxDmg}]]</div>
                </html>`
              });
            } else if (dieNum == 1){
              adv.toMessage({
                speaker: ChatMessage.getSpeaker(),
                flavor: `<html>
                <div style="min-height:50px;"><img style="width:50px; height:50px; float:left; margin-right:5px; margin-top:3px;" src="${item.img}"> <h4 style="font-size: large;"><b>${actorName}</b> attacks with their <b>${item.name}</b></h4>
                <hr>
                <i>${itemData.properties}</i></div>
                <div><b style="color: red;">FUMBLE: REROLL AGAINST ALLY IN MELEE</b></div>
                </html>`
              });
            } else {
              adv.toMessage({
                speaker: ChatMessage.getSpeaker(),
                flavor: `<html>
                <div style="min-height:50px;"><img style="width:50px; height:50px; float:left; margin-right:5px; margin-top:3px;" src="${item.img}"> <h4 style="font-size: large;"><b>${actorName}</b> attacks with their <b>${item.name}</b></h4>
                <hr>
                <i>${itemData.properties}</i></div>
                <button class="ranged-damage" data-dex="${dataDex}" data-dmg="${itemData.damage}">Roll Damage</button>
                </html>`
              });
            }
          }
          break;
        default: ui.notifications.warn("Please set your weapon's ~Weapon Type~ to Melee or Ranged.")
      }
    });

    // Delete Inventory Item
    html.find('.item-delete').click(ev => {
      const li = $(ev.currentTarget).parents(".item");
      const item = this.actor.items.get(li.data("itemId"));
      if ( item ) return item.delete();
      // this.actor.deleteEmbeddedDocuments(li.data("itemId"));
      li.slideUp(200, () => this.render(false));
    });

    // Rollable abilities.
    html.find('.rollable').click(this._onRoll.bind(this));
    html.find('.rollable-luck').click(this._onRollLuck.bind(this));
    html.find('.npc-luck').click(this._onNpcLuck.bind(this));
    html.find('.rollable-ddm').click(this._onRollDDM.bind(this));
    html.find('.custom-skill').click(this._onCustomSkill.bind(this));
    html.find('#short-rest').click(this._onShortRest.bind(this));
    html.find('#long-rest').click(this._onLongRest.bind(this));
    html.find('.npc-attack').click(this._onNpcAttack.bind(this));

    // Drag events for macros.
    if (this.actor.isOwner) {
      let handler = ev => this._onDragItemStart(ev);
      html.find('li.item').each((i, li) => {
        if (li.classList.contains("inventory-header")) return;
        li.setAttribute("draggable", true);
        li.addEventListener("dragstart", handler, false);
      });
    }

    // Rollable NPC HD and NoAppear
    html.find('.npc-rollable').click(this._npcRoll.bind(this));
  }

  /**
   * Handle creating a new Owned Item for the actor using initial data defined in the HTML dataset
   * @param {Event} event   The originating click event
   * @private
   */
  _onItemCreate(event) {
    event.preventDefault();
    const header = event.currentTarget;
    // Get the type of item to create.
    const type = header.dataset.type;
    // Grab any data associated with this control.
    const data = duplicate(header.dataset);
    // Initialize a default name.
    const name = `New ${type.capitalize()}`;
    // Prepare the item object.
    const itemData = {
      name: name,
      type: type,
      data: data
    };
    // Remove the type from the dataset since it's in the itemData.type prop.
    delete itemData.data["type"];

    // Finally, create the item!
    return this.actor.createEmbeddedDocuments("Item", [itemData]);
  }

  getLuckModifer(){
    const modifier = this.element.find('#luck-modifier')[0].value;
    if (!modifier){
      return 0;
    }
    return this.actor.system.abilities[modifier].mod;
  }
  async roll(data) {
    const result = await lfgRoll(data);
    result.roll
      .toMessage({ 
        speaker: ChatMessage.getSpeaker({ actor: this.actor }), 
        flavor: successLevelMessage(result) });
    return result;
  }
  async magicRoll(data) {
    const result = await ddmRoll(data);
    result.roll
      .toMessage({
        speaker: ChatMessage.getSpeaker({ actor: this.actor }),
        flavor: ddmMessage(result)
      });
    return result;
  }
  async incrementDDM(){
    if (this.actor.system.effects.ddm.value <= 0){
      return false;
    }
    try{
      await this.actor.update({[`system.effects.ddm.value`]: this.actor.system.effects.ddm.value + 1}); // Updates one EmbeddedEntity
    } catch(e){
      console.log(e)
    }
  }
  async resetDDM(){
    if (this.actor.system.effects.ddm.value <= 0){
      return false;
    }
    try{
      await this.actor.update({[`system.effects.ddm.value`]: 1}); // Updates one EmbeddedEntity
    } catch(e) {
      console.log(e)
    }
  }
  async decrementLuck(){
    if (this.actor.system.resources.luck.value <= 0){
      return false;
    }
    try{
      await this.actor.update({[`system.resources.luck.value`]: this.actor.system.resources.luck.value - 1}); // Updates one EmbeddedEntity
    } catch(e){
      console.log(e)
    }
  }
  /**
   * Handle clickable rolls.
   * @param {Event} event   The originating click event
   * @private
   */

  _onRollLuck(event) {
    event.preventDefault();

    let luckMod = this.getLuckModifer();
    let clicked = this;

    //add dialog for modifiers, roll from dialog
    const modDialog = new Dialog({
      title: "Situational Modifiers",
      content: `<html>
        <table class="section-table">
        <tr>
          <td><label for="sitMod">Situational Modifier</label></td>
          <td><input class="sitMod" type="text" id="sitMod" placeholder="e.g.: 1 or -2"></td>
        </tr>
        </table>
        </html>`,
      buttons: {
        button1: {
          label: "Disadvantage",
          callback: (html) => disadRoll(html)
        },
        button2: {
          label: "Normal Roll",
          callback: (html) => normalRoll(html)
        },
        button3: {
          label: "Advantage",
          callback: (html) => advRoll(html)
        }
      },
      default: "button2"
    }).render(true);

    async function disadRoll(html) {
      let sitMod = html.find("input#sitMod").val();
      let disad = true;
      const result = await clicked.roll({
        ...event.currentTarget.dataset,
        mod: luckMod,
        disad,
        sitMod
      });
      console.log(result);
      if (result.roll.total <= result.target) {
        clicked.decrementLuck();
      }
    }

    async function normalRoll(html) {
      let sitMod = html.find("input#sitMod").val();
      const result = await clicked.roll({
        ...event.currentTarget.dataset,
        mod: luckMod,
        sitMod
      });
      console.log(result);
      if (result.roll.total <= result.target) {
        clicked.decrementLuck();
      }
    }

    async function advRoll(html) {
      let sitMod = html.find("input#sitMod").val();
      let adv = true;
      const result = await clicked.roll({
        ...event.currentTarget.dataset,
        mod: luckMod,
        adv,
        sitMod
      });
      console.log(result);
      if (result.roll.total <= result.target) {
        clicked.decrementLuck();
      }
    }
  }
  _onNpcLuck(event) {
    event.preventDefault();
    let luckMod = this.getLuckModifer();
    let clicked = this;

    //add dialog for modifiers, roll from dialog
    const modDialog = new Dialog({
      title: "Situational Modifiers",
      content: `<html>
        <table class="section-table">
        <tr>
          <td><label for="sitMod">Situational Modifier</label></td>
          <td><input class="sitMod" type="text" id="sitMod" placeholder="e.g.: 1 or -2"></td>
        </tr>
        </table>
        </html>`,
      buttons: {
        button1: {
          label: "Disadvantage",
          callback: (html) => disadRoll(html)
        },
        button2: {
          label: "Normal Roll",
          callback: (html) => normalRoll(html)
        },
        button3: {
          label: "Advantage",
          callback: (html) => advRoll(html)
        }
      },
      default: "button2"
    }).render(true);

    async function disadRoll(html) {
      let sitMod = html.find("input#sitMod").val();
      let disad = true;
      const result = await clicked.roll({
        ...event.currentTarget.dataset,
        mod: luckMod,
        disad,
        sitMod
      });
      console.log(result);
    }

    async function normalRoll(html) {
      let sitMod = html.find("input#sitMod").val();
      const result = await clicked.roll({
        ...event.currentTarget.dataset,
        mod: luckMod,
        sitMod
      });
      console.log(result);
    }

    async function advRoll(html) {
      let sitMod = html.find("input#sitMod").val();
      let adv = true;
      const result = await clicked.roll({
        ...event.currentTarget.dataset,
        mod: luckMod,
        adv,
        sitMod
      });
      console.log(result);
    }
  }
  _onRoll(event) {
    event.preventDefault();
    let clicked = this;

    //add dialog for modifiers, roll from dialog
    const modDialog = new Dialog({
      title: "Situational Modifiers",
      content: `<html>
        <table class="section-table">
        <tr>
          <td><label for="sitMod">Situational Modifier</label></td>
          <td><input class="sitMod" type="text" id="sitMod" placeholder="e.g.: 1 or -2"></td>
        </tr>
        </table>
        </html>`,
      buttons: {
        button1: {
          label: "Disadvantage",
          callback: (html) => disadRoll(html)
        },
        button2: {
          label: "Normal Roll",
          callback: (html) => normalRoll(html)
        },
        button3: {
          label: "Advantage",
          callback: (html) => advRoll(html)
        }
      },
      default: "button2"
    }).render(true);

    async function disadRoll(html) {
      let sitMod = html.find("input#sitMod").val();
      let disad = true;
      const result = await clicked.roll({
        ...event.currentTarget.dataset,
        disad,
        sitMod
      });
      console.log(result);
    }

    async function normalRoll(html) {
      let sitMod = html.find("input#sitMod").val();
      const result = await clicked.roll({
        ...event.currentTarget.dataset,
        sitMod
      });
      console.log(result);
    }

    async function advRoll(html) {
      let sitMod = html.find("input#sitMod").val();
      let adv = true;
      const result = await clicked.roll({
        ...event.currentTarget.dataset,
        adv,
        sitMod
      });
      console.log(result);
    }
  }

  async _onRollDDM(event) {
    event.preventDefault();
    const result = await this.magicRoll(event.currentTarget.dataset);
    if (result.roll.total > result.target ){
      this.incrementDDM();
    } else {
      this.resetDDM();
    }
  }

  // async _onToggleSkillProficiency(event) {
  //   event.preventDefault();
  //   const name = event.currentTarget.dataset.skill
  //   try{
  //     await this.actor.updateSource({[`system.skills.${name}.skilled`]: !this.actor.system.skills[name].skilled}); // Updates one EmbeddedEntity
  //   } catch(e){
  //     console.log(e)
  //   }
  // }

  async _npcRoll(event) {
    event.preventDefault();
    const element = event.currentTarget;
    const dataset = element.dataset;

    if (dataset.roll) {
      let label = dataset.label ? `${dataset.label}` : '';
      let roll = await new Roll(dataset.roll, this.actor.system).evaluate();
      roll.toMessage({
        speaker: ChatMessage.getSpeaker({ actor: this.actor }),
        flavor: label
      });
      if (dataset.label == "Hit Dice") {
        await this.actor.update({[`system.health.max`]: roll.total});
        await this.actor.update({[`system.health.value`]: roll.total});
      }
    }
  }

  _onCustomSkill(event) {
    event.preventDefault();
    const li = this.element.find(event.currentTarget).parents(".item");
    const item = this.actor.items.get(li.data("itemId"));
    let attr = event.currentTarget.dataset.value;
    let skilled = event.currentTarget.dataset.skilled;
    let label = item.name;
    let value;

    switch (attr) {
      case 'str':
        if (skilled == "true") {
          value = (this.actor.system.abilities.str.value + 1);
        }
        if (skilled == "false") {
          value = this.actor.system.abilities.str.value;
        }
        break;
      case 'dex':
        if (skilled == "true") {
          value = (this.actor.system.abilities.dex.value + 1);
        }
        if (skilled == "false") {
          value = this.actor.system.abilities.dex.value;
        }
        break;
      case 'con':
        if (skilled == "true") {
          value = (this.actor.system.abilities.con.value + 1);
        } 
        if (skilled == "false") {
          value = this.actor.system.abilities.con.value;
        }
        break;
      case 'int':
        if (skilled == "true") {
          value = (this.actor.system.abilities.int.value + 1);
        } 
        if (skilled == "false") {
          value = this.actor.system.abilities.int.value;
        }
        break;
      case 'will':
        if (skilled == "true") {
          value = (this.actor.system.abilities.will.value + 1);
        } 
        if (skilled == "false") {
          value = this.actor.system.abilities.will.value;
        }
        break;
      case 'perc':
        if (skilled == "true") {
          value = (this.actor.system.abilities.perc.value + 1);
        } 
        if (skilled == "false") {
          value = this.actor.system.abilities.perc.value;
        }
        break;
      case 'cha':
        if (skilled == "true") {
          value = (this.actor.system.abilities.cha.value + 1);
        } 
        if (skilled == "false") {
          value = this.actor.system.abilities.cha.value;
        }
        break;
    }

    console.log(skilled);

    let clicked = this;

    //add dialog for modifiers, roll from dialog
    const modDialog = new Dialog({
      title: "Situational Modifiers",
      content: `<html>
        <table class="section-table">
        <tr>
          <td><label for="sitMod">Situational Modifier</label></td>
          <td><input class="sitMod" type="text" id="sitMod" placeholder="e.g.: 1 or -2"></td>
        </tr>
        </table>
        </html>`,
      buttons: {
        button1: {
          label: "Disadvantage",
          callback: (html) => disadRoll(html)
        },
        button2: {
          label: "Normal Roll",
          callback: (html) => normalRoll(html)
        },
        button3: {
          label: "Advantage",
          callback: (html) => advRoll(html)
        }
      },
      default: "button2"
    }).render(true);

    async function disadRoll(html) {
      let sitMod = html.find("input#sitMod").val();
      let disad = true;
      const result = await clicked.roll({
        ...event.currentTarget.dataset,
        disad,
        value,
        sitMod
      });
      console.log(result);
    }

    async function normalRoll(html) {
      let sitMod = html.find("input#sitMod").val();
      const result = await clicked.roll({
        ...event.currentTarget.dataset,
        value,
        sitMod
      });
      console.log(result);
    }

    async function advRoll(html) {
      let sitMod = html.find("input#sitMod").val();
      let adv = true;
      const result = await clicked.roll({
        ...event.currentTarget.dataset,
        adv,
        value,
        sitMod
      });
      console.log(result);
    }

  }

  _onShortRest(event) {
    event.preventDefault();
    console.log('SHORT REST');

    //shorten
    let actor = this.actor;
    let actorData = actor.system;
    let id = actor.id;

    //get will checks remaining
    let oneWill = actorData.resources.rests.oneWill;
    let twoWill = actorData.resources.rests.twoWill;
    let threeWill = actorData.resources.rests.threeWill;
    let willMessage = "";
    if (!oneWill && !twoWill && !threeWill) {
      willMessage = "You have all of your WILL checks to choose from.";
    } else if (!oneWill && !twoWill && threeWill) {
      willMessage = "You may choose between 1x and 2x WILL checks.";
    } else if (!oneWill && twoWill && threeWill) {
      willMessage = "You may only choose 1x WILL check.";
    } else if (oneWill && !twoWill && !threeWill) {
      willMessage = "You may choose between 2x and 3x WILL checks.";
    } else if (oneWill && !twoWill && threeWill) {
      willMessage = "You may only choose 2x WILL checks.";
    } else if (oneWill && twoWill && !threeWill) {
      willMessage = "You may only choose 3x WILL checks.";
    } else {
      willMessage = "You may not rest any more during this 24 hour period.";
    }

    //create rules section
    let rulesMessage = "After at least one meaningful combat, an adventurer may spend a few minutes recovering and tending to injuries. Walking and light duties may be performed.<hr> Each time a short rest is taken, one or more WILL checks may be made. Each successful check allows the character to gain one of the following benefits:<br> 1: Recover half of any lost HP, plus CON bonus (may only be taken once per short rest).<br> 2: Recover one use of an expended class ability.<br> 3: Recover one Reroll Pool die.<hr> Only three short rests may be taken per 24 hour period. In addition to the 1, 2, or 3x WILL checks, the adventurer gains a bonus number of WILL checks equal to their CON modifier (if any, ignore negatives), which may be distributed amongst the three rests as the player wishes.";

    //bonus rests section
    let bonusMessage = `You have <b>${actorData.resources.bonusRests.value}</b> of <b>${actorData.abilities.con.mod}</b> bonus checks remaining.`;
    
    //get current and max HP, as well as Con Mod
    let currentHP = actorData.health.value;
    let maxHP = actorData.health.max;
    let conMod = actorData.abilities.con.mod;

    //get current WILL value to count successess when rolling
    let will = actorData.abilities.will.value;

    //dialog to choose how many WILL checks to use
    let dialog = new Dialog({
      title: `${actor.name} Short Rest Options`,
      content: `${rulesMessage}<hr>
                ${willMessage}<hr>
                ${bonusMessage}<hr>
                <table>
                  <tr>
                    <td>Use Bonus WILL Check?</td>
                    <td><input type="checkbox" name="bonus-will" id="bonus-will" value="bonus-will"></td>
                  </tr>
                </table>
                `,
      buttons: {
        one: {
          label: "1x WILL",
          callback: (html) => oneCheck(html)
        },
        two: {
          label: "2x WILL",
          callback: (html) => twoChecks(html)
        },
        three: {
          label: "3x WILL",
          callback: (html) => threeChecks(html)
        }
      },
      default: "one"
    });
    dialog.render(true);

    async function oneCheck(html) {
      //check if bonus used, update formula
      let rollFormula;
      let bonusCheck = html.find(':checked').val();
      if (bonusCheck == "bonus-will") {
        actor.update({"system.resources.bonusRests.value": actorData.resources.bonusRests.value - 1});
        rollFormula = `2d20cs<=${will}`;
        console.log("Updated Roll Formula to add extra check");
      } else {
        rollFormula = `1d20cs<=${will}`;
        console.log("Bonus Check is false.");
      }

      //update rests used
      actor.update({"system.resources.rests.oneWill": true});

      //roll check
      let check = await new Roll(rollFormula).evaluate();
      check.toMessage({
        speaker: ChatMessage.getSpeaker(),
        flavor: `<html>
                <img style="width: 50px; height: 50px; float: left; margin: 3px 5px 0 0;" src="${actor.img}">
                ${actor.name} takes some time to recover.<hr>
                They may gain <b>${check.total}</b> short rest benefit(s).<br>
                Choose your benefit(s).<hr>
                <button class="regain-hp"
                  data-actor="${id}"
                  data-current-hp="${currentHP}"
                  data-max-hp="${maxHP}"
                  data-con="${conMod}"
                  data-will="${will}"
                  data-bonus-checks="${actorData.resources.bonusRests.value}"
                  data-bonus-checks-max="${actorData.abilities.con.mod}">Regain 1/2 HP + CON Mod</button>
                <button class="regain-ability"
                  data-actor="${id}"
                  data-bonus-checks="${actorData.resources.bonusRests.value}"
                  data-bonus-checks-max="${actorData.abilities.con.mod}">Regain 1 Ability Use</button>
                <button class="regain-reroll"
                  data-actor="${id}"
                  data-reroll-current="${actorData.resources.reroll.value}"
                  data-reroll-max="${actorData.resources.reroll.max}"
                  data-bonus-checks="${actorData.resources.bonusRests.value}"
                  data-bonus-checks-max="${actorData.abilities.con.mod}">Regain 1 Reroll Die</button>
                <button class="bonus-rest"
                  data-actor="${id}"
                  data-will="${will}"
                  data-bonus-checks="${actorData.resources.bonusRests.value}"
                  data-bonus-checks-max="${actorData.abilities.con.mod}">Use Bonus</button>
                <hr>
                </html>`
      });
    }

    async function twoChecks(html) {
      //check if bonus used, update formula
      let rollFormula;
      let bonusCheck = html.find(':checked').val();
      if (bonusCheck == "bonus-will") {
        actor.update({"system.resources.bonusRests.value": actorData.resources.bonusRests.value - 1});
        rollFormula = `3d20cs<=${will}`;
        console.log("Updated Roll Formula to add extra check");
      } else {
        rollFormula = `2d20cs<=${will}`;
        console.log("Bonus Check is false.");
      }

      //update rests used
      actor.update({"system.resources.rests.twoWill": true});

      //roll check
      let check = await new Roll(rollFormula).evaluate();
      check.toMessage({
        speaker: ChatMessage.getSpeaker(),
        flavor: `<html>
                <img style="width: 50px; height: 50px; float: left; margin: 3px 5px 0 0;" src="${actor.img}">
                ${actor.name} takes some time to recover.<hr>
                They may gain <b>${check.total}</b> short rest benefit(s).<br>
                Choose your benefit(s).<hr>
                <button class="regain-hp"
                  data-actor="${id}"
                  data-current-hp="${currentHP}"
                  data-max-hp="${maxHP}"
                  data-con="${conMod}"
                  data-will="${will}"
                  data-bonus-checks="${actorData.resources.bonusRests.value}"
                  data-bonus-checks-max="${actorData.abilities.con.mod}">Regain 1/2 HP + CON Mod</button>
                <button class="regain-ability"
                  data-actor="${id}"
                  data-bonus-checks="${actorData.resources.bonusRests.value}"
                  data-bonus-checks-max="${actorData.abilities.con.mod}">Regain 1 Ability Use</button>
                <button class="regain-reroll"
                  data-actor="${id}"
                  data-reroll-current="${actorData.resources.reroll.value}"
                  data-reroll-max="${actorData.resources.reroll.max}"
                  data-bonus-checks="${actorData.resources.bonusRests.value}"
                  data-bonus-checks-max="${actorData.abilities.con.mod}">Regain 1 Reroll Die</button>
                <button class="bonus-rest"
                  data-actor="${id}"
                  data-will="${will}"
                  data-bonus-checks="${actorData.resources.bonusRests.value}"
                  data-bonus-checks-max="${actorData.abilities.con.mod}">Use Bonus</button>
                <hr>
                </html>`
      });
    }

    async function threeChecks(html) {
      //check if bonus used, update formula
      let rollFormula;
      let bonusCheck = html.find(':checked').val();
      if (bonusCheck == "bonus-will") {
        actor.update({"system.resources.bonusRests.value": actorData.resources.bonusRests.value - 1});
        rollFormula = `4d20cs<=${will}`;
        console.log("Updated Roll Formula to add extra check");
      } else {
        rollFormula = `3d20cs<=${will}`;
        console.log("Bonus Check is false.");
      }

      //update rests used
      actor.update({"system.resources.rests.threeWill": true});

      //roll check
      let check = await new Roll(rollFormula).evaluate();
      check.toMessage({
        speaker: ChatMessage.getSpeaker(),
        flavor: `<html>
                <img style="width: 50px; height: 50px; float: left; margin: 3px 5px 0 0;" src="${actor.img}">
                ${actor.name} takes some time to recover.<hr>
                They may gain <b>${check.total}</b> short rest benefit(s).<br>
                Choose your benefit(s).<hr>
                <button class="regain-hp"
                  data-actor="${id}"
                  data-current-hp="${currentHP}"
                  data-max-hp="${maxHP}"
                  data-con="${conMod}"
                  data-will="${will}"
                  data-bonus-checks="${actorData.resources.bonusRests.value}"
                  data-bonus-checks-max="${actorData.abilities.con.mod}">Regain 1/2 HP + CON Mod</button>
                <button class="regain-ability"
                  data-actor="${id}"
                  data-will="${will}"
                  data-bonus-checks="${actorData.resources.bonusRests.value}"
                  data-bonus-checks-max="${actorData.abilities.con.mod}">Regain 1 Ability Use</button>
                <button class="regain-reroll"
                  data-actor="${id}"
                  data-will="${will}"
                  data-reroll-current="${actorData.resources.reroll.value}"
                  data-reroll-max="${actorData.resources.reroll.max}"
                  data-bonus-checks="${actorData.resources.bonusRests.value}"
                  data-bonus-checks-max="${actorData.abilities.con.mod}">Regain 1 Reroll Die</button>
                <button class="bonus-rest"
                  data-actor="${id}"
                  data-will="${will}"
                  data-bonus-checks="${actorData.resources.bonusRests.value}"
                  data-bonus-checks-max="${actorData.abilities.con.mod}">Use Bonus</button>
                <hr>
                </html>`
      });
    }
  }

  async _onLongRest(event) {
    event.preventDefault();
    console.log('LONG REST');

    let actor = this.actor;
    let actorData = actor.system;

    //update class abilities
    let featureUpdates = [];
    for (let i of actor.items) {
      let item = i;
      if (item.type === "feature") {
        if (item.system.uses.value < item.system.uses.max) {
          featureUpdates.push({_id: i.id, 'data.uses.value': item.system.uses.max});
        }
      }
    }
    actor.updateEmbeddedDocuments("Item", featureUpdates);

    //update rerolls
    if (actorData.resources.reroll.value < actorData.resources.reroll.max) {
      actor.update({"system.resources.reroll.value": actorData.resources.reroll.max});
    }

    //update attribute point loss
    if (actorData.abilities.str.value < actorData.abilities.str.base) {
      actor.update({"system.abilities.str.value": actorData.abilities.str.base});
    }
    if (actorData.abilities.dex.value < actorData.abilities.dex.base) {
      actor.update({"system.abilities.dex.value": actorData.abilities.dex.base});
    }
    if (actorData.abilities.con.value < actorData.abilities.con.base) {
      actor.update({"system.abilities.con.value": actorData.abilities.con.base});
    }
    if (actorData.abilities.int.value < actorData.abilities.int.base) {
      actor.update({"system.abilities.int.value": actorData.abilities.int.base});
    }
    if (actorData.abilities.will.value < actorData.abilities.will.base) {
      actor.update({"system.abilities.will.value": actorData.abilities.will.base});
    }
    if (actorData.abilities.perc.value < actorData.abilities.perc.base) {
      actor.update({"system.abilities.perc.value": actorData.abilities.perc.base});
    }
    if (actorData.abilities.cha.value < actorData.abilities.cha.base) {
      actor.update({"system.abilities.cha.value": actorData.abilities.cha.base});
    }

    //recover one point of luck
    if (actorData.resources.luck.value < actorData.resources.luck.max) {
      let newValue = actorData.resources.luck.value + 1;
      actor.update({"system.resources.luck.value": newValue});
    }

    //recover HP
    if (actorData.health.value < actorData.health.max) {
      //get 1d4 + con mod
      let conMod = actorData.abilities.con.mod;
      let r = await new Roll(`1d4 + ${conMod}`).evaluate();
      let conBonus = r.total;
      r.toMessage({flavor: "1d4 + Con Modifier to add to HP Recovered."});

      let hpDiff = actorData.health.max - actorData.health.value;
      let hpAdd = Math.floor(hpDiff/2) + conBonus;
      let newValue = actorData.health.value + hpAdd;
      if (newValue > actorData.health.max) {
        newValue = actorData.health.max;
      }
      actor.update({"system.health.value": newValue});
    }

    ChatMessage.create({
      content: `<html>
                <b>Long Rest: ${actor.name}</b>
                <hr>
                Expended Class Abilities Restored.<br>
                Expended Reroll Dice Restored.<br>
                All Attribute Loss Restored.<br>
                Recovered 1 Point of Luck.<br>
                Recovered Half of Lost HP, Plus 1d4 + Con Bonus.
                </html>`
    });
  }

  async _onNpcAttack(event) {
    event.preventDefault();
    const element = event.currentTarget;
    const dataset = element.dataset;
    const actor = this.actor;

    // Get Needed Info
    let attack = dataset.attack;
    let damage = dataset.damage;
    let hd = this.actor.system.npcStats.hd;
    let natNineteen = this.actor.system.npcStats.npcNineteen;

    // Get Max Hit Bonus
    let attackBonus = 0;
    if (hd > 15) {
      attackBonus = 15;
    } else {
      attackBonus = hd;
    }

    let rollData = {attack:attack, damage:damage, attackBonus:attackBonus};

    const modDialog = new Dialog({
      title: "Situational Modifiers",
      content: `<html>
        <table class="section-table">
        <tr>
          <td><label for="sitMod">Situational Modifier</label></td>
          <td><input class="sitMod" type="text" id="sitMod" placeholder="e.g.: 1 or -2"></td>
        </tr>
        </table>
        </html>`,
      buttons: {
        button1: {
          label: "Disadvantage",
          callback: (html) => disadRoll(html)
        },
        button2: {
          label: "Normal Roll",
          callback: (html) => normRoll(html)
        },
        button3: {
          label: "Advantage",
          callback: (html) => advRoll(html)
        }
      },
      default: "button2"
    }).render(true);

    async function disadRoll(html) {
      let sitMod = html.find("input#sitMod").val();
      let rollFormula = `2d20kl + ${rollData.attackBonus}`;
      if (sitMod != "") {
        rollFormula = `2d20kl + ${rollData.attackBonus} + ${sitMod}`;
      }
      // Roll the attack
      let disadRoll = await new Roll(rollFormula).evaluate();

      // Get Result (from d20, no bonus) and Determine the Chat Message Flavor
      let result = disadRoll.result[0] + disadRoll.result[1];
      let flavor = "";
      if (result == 20) {
        let dmg = await new Roll(`${damage}`).evaluate();
        let maxDmg = (dmg.dice[0].faces * dmg.dice[0].number);
        flavor = `${actor.name} attacks with their ${attack}. <br>
          <b>NATURAL 20: CRITICAL HIT</b><br>
          ${actor.name} does max damage: ${maxDmg}`;
      } else if (result == 19) {
        flavor = `${actor.name} attacks with their ${attack}. <br>
          <b>NATURAL 19</b><br>
          ${natNineteen}<br>
          <button class="npc-damage" data-dmg="${damage}">Roll Damage</button><hr>`;
      } else if (result == 1) {
        flavor = `${actor.name} attacks with their ${attack}. <br>
        <b>NATURAL 1: FUMBLE</b><br>
        The target may take an immediate free attack if they are within melee range.`;
      } else {
        flavor = `${actor.name} attacks with their ${attack}. <br>
          <button class="npc-damage" data-dmg="${damage}">Roll Damage</button><hr>`;
      }

      disadRoll.toMessage({
        speaker: ChatMessage.getSpeaker({ actor: actor }),
        flavor: flavor
      });
    }

    async function normRoll(html) {
      let sitMod = html.find("input#sitMod").val();
      let rollFormula = `1d20 + ${rollData.attackBonus}`;
      if (sitMod != "") {
        rollFormula = `1d20 + ${rollData.attackBonus} + ${sitMod}`;
      }

      // Roll the attack
      let normRoll = await new Roll(rollFormula).evaluate();

      // Get Result (from d20, no bonus) and Determine the Chat Message Flavor
      let result = normRoll.result[0] + normRoll.result[1];
      let flavor = "";
      if (result == 20) {
        let dmg = await new Roll(`${damage}`).evaluate();
        let maxDmg = (dmg.dice[0].faces * dmg.dice[0].number);
        flavor = `${actor.name} attacks with their ${attack}. <br>
          <b>NATURAL 20: CRITICAL HIT</b><br>
          ${actor.name} does max damage: ${maxDmg}`;
      } else if (result == 19) {
        flavor = `${actor.name} attacks with their ${attack}. <br>
          <b>NATURAL 19</b><br>
          ${natNineteen}<br>
          <button class="npc-damage" data-dmg="${damage}">Roll Damage</button><hr>`;
      } else if (result == 1) {
        flavor = `${actor.name} attacks with their ${attack}. <br>
        <b>NATURAL 1: FUMBLE</b><br>
        The target may take an immediate free attack if they are within melee range.`;
      } else {
        flavor = `${actor.name} attacks with their ${attack}. <br>
          <button class="npc-damage" data-dmg="${damage}">Roll Damage</button><hr>`;
      }

      normRoll.toMessage({
        speaker: ChatMessage.getSpeaker({ actor: actor }),
        flavor: flavor
      });
    }

    async function advRoll(html) {
      let sitMod = html.find("input#sitMod").val();
      let rollFormula = `2d20kh + ${rollData.attackBonus}`;
      if (sitMod != "") {
        rollFormula = `2d20kh + ${rollData.attackBonus} + ${sitMod}`;
      }

      // Roll the attack
      let advRoll = await new Roll(rollFormula).evaluate();

      // Get Result (from d20, no bonus) and Determine the Chat Message Flavor
      let result = advRoll.result[0] + advRoll.result[1];
      let flavor = "";
      if (result == 20) {
        let dmg = await new Roll(`${damage}`).evaluate();
        let maxDmg = (dmg.dice[0].faces * dmg.dice[0].number);
        flavor = `${actor.name} attacks with their ${attack}. <br>
          <b>NATURAL 20: CRITICAL HIT</b><br>
          ${actor.name} does max damage: ${maxDmg}`;
      } else if (result == 19) {
        flavor = `${actor.name} attacks with their ${attack}. <br>
          <b>NATURAL 19</b><br>
          ${natNineteen}<br>
          <button class="npc-damage" data-dmg="${damage}">Roll Damage</button><hr>`;
      } else if (result == 1) {
        flavor = `${actor.name} attacks with their ${attack}. <br>
        <b>NATURAL 1: FUMBLE</b><br>
        The target may take an immediate free attack if they are within melee range.`;
      } else {
        flavor = `${actor.name} attacks with their ${attack}. <br>
          <button class="npc-damage" data-dmg="${damage}">Roll Damage</button><hr>`;
      }

      advRoll.toMessage({
        speaker: ChatMessage.getSpeaker({ actor: actor }),
        flavor: flavor
      });
    }
  }
}
